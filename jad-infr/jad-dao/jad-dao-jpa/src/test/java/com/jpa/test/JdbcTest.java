package com.jpa.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JdbcTest {


	public static void main(String[] args)throws Exception {
		String driver="com.mysql.jdbc.Driver";
		String dburl="jdbc:mysql://localhost:3306/mybatis";
		String user = "root";
		String pass = "root";
		
		Class.forName(driver);//加载驱动
		
		Connection conn=null;
		Statement stmt =null;
		try {
			conn = DriverManager.getConnection(dburl,user,pass);//创建连接
			stmt = conn.createStatement(); //创建 Statement
			String sql = "SELECT id, name  FROM user";
			ResultSet rs = stmt.executeQuery(sql);//执行查询
			while(rs.next()){
			    System.out.println("id:" + rs.getInt("id")
			    		+",name:"+rs.getString("name"));
			}
		} finally {
			stmt.close();
			conn.close();
		}
	}

}
