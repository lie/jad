package com.jad.dao.jpa.config;

import org.springframework.data.jpa.repository.config.JpaRepositoryConfigExtension;

import com.jad.dao.jpa.support.JadRepositoryFactoryBean;

public class JadRepositoryConfigExtension extends JpaRepositoryConfigExtension {

	public String getRepositoryFactoryClassName() {
		return JadRepositoryFactoryBean.class.getName();
	}
}
