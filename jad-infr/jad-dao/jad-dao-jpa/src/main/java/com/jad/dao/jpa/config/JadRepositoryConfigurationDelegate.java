package com.jad.dao.jpa.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.annotation.AnnotatedGenericBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;
import org.springframework.core.env.EnvironmentCapable;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.data.repository.config.AnnotationRepositoryConfigurationSource;
import org.springframework.data.repository.config.CustomRepositoryImplementationDetector;
import org.springframework.data.repository.config.NamedQueriesBeanDefinitionBuilder;
import org.springframework.data.repository.config.RepositoryBeanNameGenerator;
import org.springframework.data.repository.config.RepositoryConfiguration;
import org.springframework.data.repository.config.RepositoryConfigurationDelegate;
import org.springframework.data.repository.config.RepositoryConfigurationExtension;
import org.springframework.data.repository.config.RepositoryConfigurationSource;
import org.springframework.data.repository.config.XmlRepositoryConfigurationSource;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;
import org.springframework.data.repository.query.ExtensionAwareEvaluationContextProvider;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import com.jad.commons.utils.StringUtils;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.enums.RespositoryType;
import com.jad.dao.jpa.support.JpaDaoDelegate;

public class JadRepositoryConfigurationDelegate  {


	private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryConfigurationDelegate.class);

	private static final String REPOSITORY_REGISTRATION = "Spring Data {} - Registering repository: {} - Interface: {} - Factory: {}";
	private static final String MULTIPLE_MODULES = "Multiple Spring Data modules found, entering strict repository configuration mode!";
	private static final String MODULE_DETECTION_PACKAGE = "org.springframework.data.**.repository.support";

	static final String FACTORY_BEAN_OBJECT_TYPE = "factoryBeanObjectType";
	
	private static final String INTERFACE="Interface";
	
	private static final String COMPONENT_ANNOTATION_CLASSNAME = "org.springframework.stereotype.Component";
	
	static final String ANNOTATION_TYPE="com.jad.dao.annotation.JadDao";

	private final RepositoryConfigurationSource configurationSource;
	private final ResourceLoader resourceLoader;
	private final Environment environment;
	private final BeanNameGenerator beanNameGenerator;
	private final boolean isXml;
	private final boolean inMultiStoreMode;

	/**
	 * Creates a new {@link RepositoryConfigurationDelegate} for the given {@link RepositoryConfigurationSource} and
	 * {@link ResourceLoader} and {@link Environment}.
	 * 
	 * @param configurationSource must not be {@literal null}.
	 * @param resourceLoader must not be {@literal null}.
	 * @param environment must not be {@literal null}.
	 */
	public JadRepositoryConfigurationDelegate(RepositoryConfigurationSource configurationSource,
			ResourceLoader resourceLoader, Environment environment) {

		this.isXml = configurationSource instanceof XmlRepositoryConfigurationSource;
		boolean isAnnotation = configurationSource instanceof AnnotationRepositoryConfigurationSource;

		Assert.isTrue(isXml || isAnnotation,
				"Configuration source must either be an Xml- or an AnnotationBasedConfigurationSource!");
		Assert.notNull(resourceLoader);

		RepositoryBeanNameGenerator generator = new RepositoryBeanNameGenerator();
		generator.setBeanClassLoader(resourceLoader.getClassLoader());

		this.beanNameGenerator = generator;
		this.configurationSource = configurationSource;
		this.resourceLoader = resourceLoader;
		this.environment = defaultEnvironment(environment, resourceLoader);
		this.inMultiStoreMode = multipleStoresDetected();
	}
	
	

	/**
	 * Defaults the environment in case the given one is null. Used as fallback, in case the legacy constructor was
	 * invoked.
	 * 
	 * @param environment can be {@literal null}.
	 * @param resourceLoader can be {@literal null}.
	 * @return
	 */
	private static Environment defaultEnvironment(Environment environment, ResourceLoader resourceLoader) {

		if (environment != null) {
			return environment;
		}

		return resourceLoader instanceof EnvironmentCapable ? ((EnvironmentCapable) resourceLoader).getEnvironment()
				: new StandardEnvironment();
	}

	/**
	 * Registers the found repositories in the given {@link BeanDefinitionRegistry}.
	 * 
	 * @param registry
	 * @param extension
	 * @return {@link BeanComponentDefinition}s for all repository bean definitions found.
	 */
	public List<BeanComponentDefinition> registerRepositoriesIn(BeanDefinitionRegistry registry,
			RepositoryConfigurationExtension extension) {

		extension.registerBeansForRoot(registry, configurationSource);

//		JadRepositoryBeanDefinitionBuilder builder = new JadRepositoryBeanDefinitionBuilder(registry, extension, resourceLoader,
//				environment);
		List<BeanComponentDefinition> definitions = new ArrayList<BeanComponentDefinition>();

		for (RepositoryConfiguration<? extends RepositoryConfigurationSource> configuration : extension
				.getRepositoryConfigurations(configurationSource, resourceLoader, inMultiStoreMode)) {
			
			boolean annotationCheck=checkAnnotation(configuration);//判断注解
			if(!annotationCheck){
				LOGGER.warn("接口"+configuration.getRepositoryInterface()+"没有被JadDao注解，跳过注入");
				continue;
			}
			
//			BeanDefinitionBuilder definitionBuilder = builder.build(configuration);
			BeanDefinitionBuilder definitionBuilder = buildInterface(registry, extension,configuration);//生成接口
			
			//注册实现类
			String customImplementationBeanName = registerCustomImplementation(registry,configuration);
			if (customImplementationBeanName != null) {
				definitionBuilder.addPropertyReference("customImplementation", customImplementationBeanName);
				definitionBuilder.addDependsOn(customImplementationBeanName);
			}
			
			//注册delegate
//			if (customImplementationBeanName != null) {
//				BeanComponentDefinition delegateDefinition=registerDelegate(customImplementationBeanName,configuration,registry,extension);
//				if(delegateDefinition!=null){
//					definitions.add(delegateDefinition);
//				}
//			}
			synchronized (registry) {
				if(!registry.containsBeanDefinition(AbstractJadEntityDao.COMMON_DELETAGE_BEANNAME) 
						&& !registry.isAlias(AbstractJadEntityDao.COMMON_DELETAGE_BEANNAME) ){
					BeanComponentDefinition delegateDefinition=registerCommonDelegate(customImplementationBeanName,configuration,registry,extension);
					definitions.add(delegateDefinition);
				}
				String delegateBeanName = customImplementationBeanName+AbstractJadEntityDao.DELEGATE_BEAN_NAME_SUFFIX;
				if(!registry.containsBeanDefinition(delegateBeanName) 
						&& !registry.isAlias(delegateBeanName) ){
					registry.registerAlias(AbstractJadEntityDao.COMMON_DELETAGE_BEANNAME, delegateBeanName);
				}
			}
			
			
			
			
			extension.postProcess(definitionBuilder, configurationSource);

			if (isXml) {
				extension.postProcess(definitionBuilder, (XmlRepositoryConfigurationSource) configurationSource);
			} else {
				extension.postProcess(definitionBuilder, (AnnotationRepositoryConfigurationSource) configurationSource);
			}
			
			AbstractBeanDefinition beanDefinition = definitionBuilder.getBeanDefinition();
			
			String interfaceBeanName = beanNameGenerator.generateBeanName(beanDefinition, registry);
			
//			String delegateBeanName = customImplementationBeanName + AbstractJadEntityDao.DELEGATE_BEAN_NAME_SUFFIX;
			
			if(interfaceBeanName.equals(customImplementationBeanName)){//名称冲突了，改名
				interfaceBeanName=interfaceBeanName+INTERFACE;
			}
			
			LOGGER.debug("beanName debug:"+interfaceBeanName);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(REPOSITORY_REGISTRATION, new Object[]{extension.getModuleName(), interfaceBeanName,
						configuration.getRepositoryInterface(), extension.getRepositoryFactoryClassName()});
			}

			beanDefinition.setAttribute(FACTORY_BEAN_OBJECT_TYPE, configuration.getRepositoryInterface());
			
			registry.registerBeanDefinition(interfaceBeanName, beanDefinition);//注册
			
//			registry.registerAlias(interfaceBeanName, delegateBeanName);//注册 delegate
			
			definitions.add(new BeanComponentDefinition(beanDefinition, interfaceBeanName));
			
		}

		return definitions;
	}
	
	/**
	 * （正在测试）
	 * @param interfaceBeanName
	 * @param configuration
	 * @param registry
	 * @param extension
	 * @return
	 */
	private BeanComponentDefinition registerCommonDelegate(String interfaceBeanName, 
			RepositoryConfiguration<? extends RepositoryConfigurationSource> configuration,
			BeanDefinitionRegistry registry,
			RepositoryConfigurationExtension extension){
		
		RootBeanDefinition beanDefinition=new RootBeanDefinition(JpaDaoDelegate.class);
		beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
		beanDefinition.setLazyInit(false);
		beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
		beanDefinition.setSource(configuration.getSource());
		registry.registerBeanDefinition(AbstractJadEntityDao.COMMON_DELETAGE_BEANNAME, beanDefinition);//
		return new BeanComponentDefinition(beanDefinition, AbstractJadEntityDao.COMMON_DELETAGE_BEANNAME);
		
	}
	
	/**
	 * 注册delegate(这个可以用，但是有bug)
	 * @param interfaceBeanName
	 * @param configuration
	 * @param registry
	 * @param extension
	 * @return
	 */
	@SuppressWarnings("unused")
	private BeanComponentDefinition registerDelegate(String interfaceBeanName, 
			RepositoryConfiguration<? extends RepositoryConfigurationSource> configuration,
			BeanDefinitionRegistry registry,
			RepositoryConfigurationExtension extension){
		
		BeanDefinitionBuilder definitionBuilder = buildInterface(registry, extension,configuration);
		extension.postProcess(definitionBuilder, configurationSource);
		
		if (isXml) {
			extension.postProcess(definitionBuilder, (XmlRepositoryConfigurationSource) configurationSource);
		} else {
			extension.postProcess(definitionBuilder, (AnnotationRepositoryConfigurationSource) configurationSource);
		}
		
		AbstractBeanDefinition beanDefinition = definitionBuilder.getBeanDefinition();
		
		String beanName = interfaceBeanName+AbstractJadEntityDao.DELEGATE_BEAN_NAME_SUFFIX;

		beanDefinition.setAttribute(FACTORY_BEAN_OBJECT_TYPE, configuration.getRepositoryInterface());
		
		registry.registerBeanDefinition(beanName, beanDefinition);//
		return new BeanComponentDefinition(beanDefinition, beanName);
	}
	

	public BeanDefinitionBuilder buildInterface(BeanDefinitionRegistry registry,
			RepositoryConfigurationExtension extension,RepositoryConfiguration<?> configuration) {

		Assert.notNull(registry, "BeanDefinitionRegistry must not be null!");
		Assert.notNull(extension, "RepositoryConfigurationExtension must not be null!");
		Assert.notNull(resourceLoader, "ResourceLoader must not be null!");
		Assert.notNull(environment, "Environment must not be null!");

		String factoryBeanName = configuration.getRepositoryFactoryBeanName();
		factoryBeanName = StringUtils.isNotBlank(factoryBeanName) ? factoryBeanName : extension
				.getRepositoryFactoryClassName();

		BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(factoryBeanName);

		builder.getRawBeanDefinition().setSource(configuration.getSource());
		builder.addPropertyValue("repositoryInterface", configuration.getRepositoryInterface());
		builder.addPropertyValue("queryLookupStrategyKey", configuration.getQueryLookupStrategyKey());
		builder.addPropertyValue("lazyInit", configuration.isLazyInit());
		builder.addPropertyValue("repositoryBaseClass", configuration.getRepositoryBaseClassName());
		
		NamedQueriesBeanDefinitionBuilder definitionBuilder = new NamedQueriesBeanDefinitionBuilder(
				extension.getDefaultNamedQueryLocation());

		if (StringUtils.isNotBlank(configuration.getNamedQueriesLocation())) {
			definitionBuilder.setLocations(configuration.getNamedQueriesLocation());
		}

		builder.addPropertyValue("namedQueries", definitionBuilder.build(configuration.getSource()));

//		String customImplementationBeanName = registerCustomImplementation(configuration);
//
//		if (customImplementationBeanName != null) {
//			builder.addPropertyReference("customImplementation", customImplementationBeanName);
//			builder.addDependsOn(customImplementationBeanName);
//		}

		RootBeanDefinition evaluationContextProviderDefinition = new RootBeanDefinition(
				ExtensionAwareEvaluationContextProvider.class);
		evaluationContextProviderDefinition.setSource(configuration.getSource());

		builder.addPropertyValue("evaluationContextProvider", evaluationContextProviderDefinition);

		return builder;
	}
	
	
	private String registerCustomImplementation(BeanDefinitionRegistry registry,RepositoryConfiguration<?> configuration) {
		
		
		CachingMetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourceLoader);
		CustomRepositoryImplementationDetector implementationDetector 
				= new CustomRepositoryImplementationDetector(metadataReaderFactory, environment,resourceLoader);
		
		String beanAliasName = configuration.getImplementationBeanName();
//
		// Already a bean configured?
		if (registry.isAlias(beanAliasName) || registry.containsBeanDefinition(beanAliasName)  ) {
			return beanAliasName;
		}
		
		AbstractBeanDefinition beanDefinition = implementationDetector.detectCustomImplementation(
				configuration.getImplementationClassName(), configuration.getBasePackages());
		
		if (null == beanDefinition) {
			return null;
		}
		
		String implClassName=beanDefinition.getBeanClassName();
		Class implClass=null;
		try {
			implClass=ClassUtils.forName(implClassName, configuration.getClass().getClassLoader());
		} catch (Exception e) {
			LOGGER.warn("无法加载实现类"+implClassName+",跳过,"+e.getMessage(),e);
			return null;
		}
		
		AnnotatedBeanDefinition annotatedBeanDefinition = new AnnotatedGenericBeanDefinition(implClass);
		String realBeanName=determineBeanNameFromAnnotation(annotatedBeanDefinition);//查看注解上的名称
		if(realBeanName==null || "".equals(realBeanName.trim())){
			realBeanName=beanAliasName;
		}
		
		beanDefinition.getPropertyValues().add("repositoryType", RespositoryType.JPA.getRespositoryType());
		beanDefinition.setSource(configuration.getSource());
		
		if(!realBeanName.equals(beanAliasName)){
			if(registry.containsBeanDefinition(realBeanName)){
				
				registry.getBeanDefinition(realBeanName).getPropertyValues().add("repositoryType", RespositoryType.JPA.getRespositoryType());
				
				registry.registerAlias(realBeanName, beanAliasName);//注册实现类别名，以防止下次重复判断，提升性能
				return realBeanName;
			}
		}
			
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Registering custom repository implementation: " + configuration.getImplementationBeanName() + " "
					+ beanDefinition.getBeanClassName());
		}
		registry.registerBeanDefinition(realBeanName, beanDefinition);////注册实现类
		
		if(!realBeanName.equals(beanAliasName)){
			registry.registerAlias(realBeanName, beanAliasName);
		}
		
		return realBeanName;
	}
	
	
	protected String determineBeanNameFromAnnotation(AnnotatedBeanDefinition annotatedDef) {
		if(annotatedDef==null)return null;
		AnnotationMetadata amd = annotatedDef.getMetadata();
		Set<String> types = amd.getAnnotationTypes();
		String beanName = null;
		for (String type : types) {
			AnnotationAttributes attributes = AnnotationAttributes.fromMap(amd.getAnnotationAttributes(type, false));
			if (isStereotypeWithNameValue(type, amd.getMetaAnnotationTypes(type), attributes)) {
				Object value = attributes.get("value");
				if (value instanceof String) {
					String strVal = (String) value;
					if (StringUtils.isNotBlank(strVal)) {
						if (beanName != null && !strVal.equals(beanName)) {
							throw new IllegalStateException("Stereotype annotations suggest inconsistent " +
									"component names: '" + beanName + "' versus '" + strVal + "'");
						}
						beanName = strVal;
					}
				}
			}
		}
		return beanName;
	}
	
	protected boolean isStereotypeWithNameValue(String annotationType,
			Set<String> metaAnnotationTypes, Map<String, Object> attributes) {

		boolean isStereotype = annotationType.equals(COMPONENT_ANNOTATION_CLASSNAME) ||
				(metaAnnotationTypes != null && metaAnnotationTypes.contains(COMPONENT_ANNOTATION_CLASSNAME)) ||
				annotationType.equals("javax.annotation.ManagedBean") ||
				annotationType.equals("javax.inject.Named");

		return (isStereotype && attributes != null && attributes.containsKey("value"));
	}
	
	
	@SuppressWarnings("unused")
	private boolean checkAnnotation(RepositoryConfiguration<? extends RepositoryConfigurationSource> configuration){
		
		String interfaceName=configuration.getRepositoryInterface();
		
		try {
			Class classz=ClassUtils.forName(interfaceName, this.getClass().getClassLoader());
			AnnotatedBeanDefinition annotatedDef = new AnnotatedGenericBeanDefinition(classz);
			AnnotationMetadata amd = annotatedDef.getMetadata();
			
			Set<String>annotationTypes=amd.getAnnotationTypes();
			
			for(String annotation:annotationTypes){
				if(ANNOTATION_TYPE.equals(annotation)){
					return true;
				}
			}
		}catch (Exception e) {
			LOGGER.warn("加载接口["+interfaceName+"]失败,"+e.getMessage(),e);
		}
		return false;
	}

	
	
	
	
	
	
	
	

	/**
	 * Scans {@code repository.support} packages for implementations of {@link RepositoryFactorySupport}. Finding more
	 * than a single type is considered a multi-store configuration scenario which will trigger stricter repository
	 * scanning.
	 * 
	 * @return
	 */
	private boolean multipleStoresDetected() {

		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
		scanner.setEnvironment(environment);
		scanner.setResourceLoader(resourceLoader);
		scanner.addIncludeFilter(new AssignableTypeFilter(RepositoryFactorySupport.class));

		if (scanner.findCandidateComponents(MODULE_DETECTION_PACKAGE).size() > 1) {

			LOGGER.info(MULTIPLE_MODULES);
			return true;
		}

		return false;
	}

	
}
