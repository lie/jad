package com.jad.dao.jpa.config;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;
import org.springframework.data.jpa.repository.config.AuditingBeanDefinitionParser;
import org.springframework.data.jpa.repository.config.JpaRepositoryConfigExtension;
import org.springframework.data.repository.config.RepositoryBeanDefinitionParser;


public class JadRepositoryNameSpaceHandler extends NamespaceHandlerSupport {


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.beans.factory.xml.NamespaceHandler#init()
	 */
	public void init() {

		JadRepositoryConfigExtension extension = new JadRepositoryConfigExtension();
		RepositoryBeanDefinitionParser repositoryBeanDefinitionParser = new JadRepositoryBeanDefinitionParser(extension);

		registerBeanDefinitionParser("repositories", repositoryBeanDefinitionParser);
		registerBeanDefinitionParser("auditing", new AuditingBeanDefinitionParser());
	}


}
