package com.jad.dao.mybatis.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jad.dao.mybatis.eo.TestDictEo;
import com.jad.dao.mybatis.service.TestDictService;


public class DictServiceTestCase {

	
	private static Logger logger=LoggerFactory.getLogger(DictServiceTestCase.class); 
	
	protected ApplicationContext context ;
	
//	private String contextConfigFile="spring-mybatis-test.xml";
	private String contextConfigFile="spring-mybatis-uncode.xml";
	
	private TestDictService testDictService;
	
//	private DefaultMasterSlaveRouter masterSlaveRouter = new DefaultMasterSlaveRouter();
	
	@Test
	public void findById(){
//		masterSlaveRouter.routeToMaster();
//		masterSlaveRouter.routeToSlave();
		String id="1";
		TestDictEo eo = testDictService.findById(id);
		System.out.println("测试完成。。。,"+eo.getLabel());
	}
	
	
	
	
	@BeforeTest
    public void baseBeforeTest() throws Exception {
		logger.debug("context 正在初始化...");
		context = new ClassPathXmlApplicationContext(getContextConfigFile());
		testDictService = (TestDictService)context.getBean("testDictService");
		logger.debug("context 初始化完成...");
	}
	
	public String getContextConfigFile() {
		return contextConfigFile;
	}

}
