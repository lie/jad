package com.jad.dao.mybatis.mapper;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.ibatis.binding.MapperProxy;
import org.apache.ibatis.session.SqlSession;

public class JadMapperProxyFactory<T>   {
	


	  private final Class<T> mapperInterface;
	  private final Map<Method, JadMapperMethod> methodCache = new ConcurrentHashMap<Method, JadMapperMethod>();

	  public JadMapperProxyFactory(Class<T> mapperInterface) {
	    this.mapperInterface = mapperInterface;
	  }

	  public Class<T> getMapperInterface() {
	    return mapperInterface;
	  }

	  public Map<Method, JadMapperMethod> getMethodCache() {
	    return methodCache;
	  }

	  @SuppressWarnings("unchecked")
	  protected T newInstance(JadMapperProxy<T> mapperProxy) {
	    return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[] { mapperInterface }, mapperProxy);
	  }

	  public T newInstance(SqlSession sqlSession) {
	    final JadMapperProxy<T> mapperProxy = new JadMapperProxy<T>(sqlSession, mapperInterface, methodCache);
	    return newInstance(mapperProxy);
	  }


}
