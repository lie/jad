package com.jad.dao.mybatis.conf;

import static org.springframework.util.Assert.notNull;
import static org.springframework.util.StringUtils.tokenizeToStringArray;

import java.lang.annotation.Annotation;
import java.util.Map;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.config.PropertyResourceConfigurer;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.config.TypedStringValue;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.util.StringUtils;

import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.enums.RespositoryType;

public class JadMapperScannerConfigurer extends MapperScannerConfigurer {
	

	private static final Logger logger = LoggerFactory.getLogger(JadMapperScannerConfigurer.class);
	
	  private String basePackage;

	  private boolean addToConfig = true;

	  private SqlSessionFactory sqlSessionFactory;

	  private SqlSessionTemplate sqlSessionTemplate;

	  private String sqlSessionFactoryBeanName;

	  private String sqlSessionTemplateBeanName;

	  private Class<? extends Annotation> annotationClass;

	  private Class<?> markerInterface;

	  private ApplicationContext applicationContext;

	  private String beanName;
	  
	  private String implPostfix;

	  private boolean processPropertyPlaceHolders;

	  private BeanNameGenerator nameGenerator;

	  /**
	   * This property lets you set the base package for your mapper interface files.
	   * <p>
	   * You can set more than one package by using a semicolon or comma as a separator.
	   * <p>
	   * Mappers will be searched for recursively starting in the specified package(s).
	   *
	   * @param basePackage base package name
	   */
	  public void setBasePackage(String basePackage) {
	    this.basePackage = basePackage;
	  }

	  /**
	   * Same as {@code MapperFactoryBean#setAddToConfig(boolean)}.
	   *
	   * @param addToConfig
	   * @see MapperFactoryBean#setAddToConfig(boolean)
	   */
	  public void setAddToConfig(boolean addToConfig) {
	    this.addToConfig = addToConfig;
	  }

	  /**
	   * This property specifies the annotation that the scanner will search for.
	   * <p>
	   * The scanner will register all interfaces in the base package that also have the
	   * specified annotation.
	   * <p>
	   * Note this can be combined with markerInterface.
	   *
	   * @param annotationClass annotation class
	   */
	  public void setAnnotationClass(Class<? extends Annotation> annotationClass) {
	    this.annotationClass = annotationClass;
	  }

	  /**
	   * This property specifies the parent that the scanner will search for.
	   * <p>
	   * The scanner will register all interfaces in the base package that also have the
	   * specified interface class as a parent.
	   * <p>
	   * Note this can be combined with annotationClass.
	   *
	   * @param superClass parent class
	   */
	  public void setMarkerInterface(Class<?> superClass) {
	    this.markerInterface = superClass;
	  }

	  /**
	   * Specifies which {@code SqlSessionTemplate} to use in the case that there is
	   * more than one in the spring context. Usually this is only needed when you
	   * have more than one datasource.
	   * <p>
	   * Use {@link #setSqlSessionTemplateBeanName(String)} instead
	   *
	   * @param sqlSessionTemplate
	   */
	  @Deprecated
	  public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
	    this.sqlSessionTemplate = sqlSessionTemplate;
	  }

	  /**
	   * Specifies which {@code SqlSessionTemplate} to use in the case that there is
	   * more than one in the spring context. Usually this is only needed when you
	   * have more than one datasource.
	   * <p>
	   * Note bean names are used, not bean references. This is because the scanner
	   * loads early during the start process and it is too early to build mybatis
	   * object instances.
	   *
	   * @since 1.1.0
	   *
	   * @param sqlSessionTemplateName Bean name of the {@code SqlSessionTemplate}
	   */
	  public void setSqlSessionTemplateBeanName(String sqlSessionTemplateName) {
	    this.sqlSessionTemplateBeanName = sqlSessionTemplateName;
	  }

	  /**
	   * Specifies which {@code SqlSessionFactory} to use in the case that there is
	   * more than one in the spring context. Usually this is only needed when you
	   * have more than one datasource.
	   * <p>
	   * Use {@link #setSqlSessionFactoryBeanName(String)} instead.
	   *
	   * @param sqlSessionFactory
	   */
	  @Deprecated
	  public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
	    this.sqlSessionFactory = sqlSessionFactory;
	  }

	  /**
	   * Specifies which {@code SqlSessionFactory} to use in the case that there is
	   * more than one in the spring context. Usually this is only needed when you
	   * have more than one datasource.
	   * <p>
	   * Note bean names are used, not bean references. This is because the scanner
	   * loads early during the start process and it is too early to build mybatis
	   * object instances.
	   *
	   * @since 1.1.0
	   *
	   * @param sqlSessionFactoryName Bean name of the {@code SqlSessionFactory}
	   */
	  public void setSqlSessionFactoryBeanName(String sqlSessionFactoryName) {
	    this.sqlSessionFactoryBeanName = sqlSessionFactoryName;
	  }

	  /**
	   *
	   * @since 1.1.1
	   *
	   * @param processPropertyPlaceHolders
	   */
	  public void setProcessPropertyPlaceHolders(boolean processPropertyPlaceHolders) {
	    this.processPropertyPlaceHolders = processPropertyPlaceHolders;
	  }

	  /**
	   * {@inheritDoc}
	   */
	  public void setApplicationContext(ApplicationContext applicationContext) {
	    this.applicationContext = applicationContext;
	  }

	  /**
	   * {@inheritDoc}
	   */
	  public void setBeanName(String name) {
	    this.beanName = name;
	  }

	  /**
	   * Gets beanNameGenerator to be used while running the scanner.
	   *
	   * @return the beanNameGenerator BeanNameGenerator that has been configured
	   * @since 1.2.0
	   */
	  public BeanNameGenerator getNameGenerator() {
	    return nameGenerator;
	  }

	  /**
	   * Sets beanNameGenerator to be used while running the scanner.
	   *
	   * @param nameGenerator the beanNameGenerator to set
	   * @since 1.2.0
	   */
	  public void setNameGenerator(BeanNameGenerator nameGenerator) {
	    this.nameGenerator = nameGenerator;
	  }

	  /**
	   * {@inheritDoc}
	   */
	  public void afterPropertiesSet() throws Exception {
	    notNull(this.basePackage, "Property 'basePackage' is required");
	  }

	  /**
	   * {@inheritDoc}
	   */
	  public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
	    // left intentionally blank
	  }

	  /**
	   * {@inheritDoc}
	   * 
	   * @since 1.0.2
	   */
	  public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
	    if (this.processPropertyPlaceHolders) {
	      processPropertyPlaceHolders();
	    }
	    
	  //注入持久化类型
	    if(!registry.containsBeanDefinition(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME) 
	    		&& !registry.isAlias(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME) ){
			RootBeanDefinition beanDefinition=new RootBeanDefinition(String.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			ConstructorArgumentValues constructorArgumentValues=new ConstructorArgumentValues();
			constructorArgumentValues.addIndexedArgumentValue(0, RespositoryType.MYBATIS.getRespositoryType());
			beanDefinition.setConstructorArgumentValues(constructorArgumentValues);
			registry.registerBeanDefinition(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME, beanDefinition);//注册 repositoryType
		}

	    JadClassPathMapperScanner scanner = new JadClassPathMapperScanner(registry);//换一个扫描器
	    scanner.setAddToConfig(this.addToConfig);
	    scanner.setAnnotationClass(this.annotationClass);
	    scanner.setMarkerInterface(this.markerInterface);
	    scanner.setSqlSessionFactory(this.sqlSessionFactory);
	    scanner.setSqlSessionTemplate(this.sqlSessionTemplate);
	    scanner.setSqlSessionFactoryBeanName(this.sqlSessionFactoryBeanName);
	    scanner.setSqlSessionTemplateBeanName(this.sqlSessionTemplateBeanName);
	    scanner.setResourceLoader(this.applicationContext);
	    scanner.setApplicationContext(this.applicationContext);
	    scanner.setBeanNameGenerator(this.nameGenerator);
	    
	    if(this.implPostfix==null){
	    	this.implPostfix=AbstractJadEntityDao.DEFAULT_IMPL_SUFFIX;
	    }
	    scanner.setImplPostfix(this.implPostfix);//实现类后缀
	    
	    scanner.registerFilters();
	    scanner.scan(tokenizeToStringArray(this.basePackage, ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS));
	    
	  }
	  
	  
	  public RootBeanDefinition newJadJaoBeanDefinition(String beanName){
			
//			GenericBeanDefinition definition = (GenericBeanDefinition) srcdefinition;
			
			RootBeanDefinition definition=new RootBeanDefinition();
			definition.setScope(BeanDefinition.SCOPE_SINGLETON);
			definition.setLazyInit(false);
			definition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			
			String beanClassName = "com.jad.dao.JadEntityDao";
			
	        if (logger.isDebugEnabled()) {
	          logger.debug("Creating MapperFactoryBean with name '" + beanName  
	              + "' and '" + beanClassName + "' mapperInterface");
	        }

	        // the mapper interface is the original class of the bean
	        // but, the actual class of the bean is MapperFactoryBean
	        definition.getPropertyValues().add("mapperInterface", beanClassName);
	        
	        definition.setBeanClass(MapperFactoryBean.class);
	        
	        definition.getPropertyValues().add("addToConfig", this.addToConfig);

	        boolean explicitFactoryUsed = false;
	        if (StringUtils.hasText(this.sqlSessionFactoryBeanName)) {
	          definition.getPropertyValues().add("sqlSessionFactory", new RuntimeBeanReference(this.sqlSessionFactoryBeanName));
	          explicitFactoryUsed = true;
	        } else if (this.sqlSessionFactory != null) {
	          definition.getPropertyValues().add("sqlSessionFactory", this.sqlSessionFactory);
	          explicitFactoryUsed = true;
	        }

	        if (StringUtils.hasText(this.sqlSessionTemplateBeanName)) {
	          if (explicitFactoryUsed) {
	            logger.warn("Cannot use both: sqlSessionTemplate and sqlSessionFactory together. sqlSessionFactory is ignored.");
	          }
	          definition.getPropertyValues().add("sqlSessionTemplate", new RuntimeBeanReference(this.sqlSessionTemplateBeanName));
	          explicitFactoryUsed = true;
	        } else if (this.sqlSessionTemplate != null) {
	          if (explicitFactoryUsed) {
	            logger.warn("Cannot use both: sqlSessionTemplate and sqlSessionFactory together. sqlSessionFactory is ignored.");
	          }
	          definition.getPropertyValues().add("sqlSessionTemplate", this.sqlSessionTemplate);
	          explicitFactoryUsed = true;
	        }

	        if (!explicitFactoryUsed) {
	          if (logger.isDebugEnabled()) {
	            logger.debug("Enabling autowire by type for MapperFactoryBean with name '" + beanName + "'.");
	          }
	          definition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
	        }
	        return definition;
		}

	  /*
	   * BeanDefinitionRegistries are called early in application startup, before
	   * BeanFactoryPostProcessors. This means that PropertyResourceConfigurers will not have been
	   * loaded and any property substitution of this class' properties will fail. To avoid this, find
	   * any PropertyResourceConfigurers defined in the context and run them on this class' bean
	   * definition. Then update the values.
	   */
	  private void processPropertyPlaceHolders() {
	    Map<String, PropertyResourceConfigurer> prcs = applicationContext.getBeansOfType(PropertyResourceConfigurer.class);

	    if (!prcs.isEmpty() && applicationContext instanceof GenericApplicationContext) {
	      BeanDefinition mapperScannerBean = ((GenericApplicationContext) applicationContext)
	          .getBeanFactory().getBeanDefinition(beanName);

	      // PropertyResourceConfigurer does not expose any methods to explicitly perform
	      // property placeholder substitution. Instead, create a BeanFactory that just
	      // contains this mapper scanner and post process the factory.
	      DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
	      factory.registerBeanDefinition(beanName, mapperScannerBean);

	      for (PropertyResourceConfigurer prc : prcs.values()) {
	        prc.postProcessBeanFactory(factory);
	      }

	      PropertyValues values = mapperScannerBean.getPropertyValues();

	      this.basePackage = updatePropertyValue("basePackage", values);
	      this.sqlSessionFactoryBeanName = updatePropertyValue("sqlSessionFactoryBeanName", values);
	      this.sqlSessionTemplateBeanName = updatePropertyValue("sqlSessionTemplateBeanName", values);
	    }
	  }

	  private String updatePropertyValue(String propertyName, PropertyValues values) {
	    PropertyValue property = values.getPropertyValue(propertyName);

	    if (property == null) {
	      return null;
	    }

	    Object value = property.getValue();

	    if (value == null) {
	      return null;
	    } else if (value instanceof String) {
	      return value.toString();
	    } else if (value instanceof TypedStringValue) {
	      return ((TypedStringValue) value).getValue();
	    } else {
	      return null;
	    }
	  }

	public void setImplPostfix(String implPostfix) {
		this.implPostfix = implPostfix;
	}


}
