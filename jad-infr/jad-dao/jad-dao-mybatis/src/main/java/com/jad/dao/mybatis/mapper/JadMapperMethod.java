package com.jad.dao.mybatis.mapper;


import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Flush;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.binding.BindingException;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.ParamNameResolver;
import org.apache.ibatis.reflection.TypeParameterResolver;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;

import com.jad.dao.mybatis.reflection.JadParamNameResolver;


import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Flush;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.binding.BindingException;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.TypeParameterResolver;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.mybatis.interceptor.JadAbstractInterceptor;
import com.jad.dao.mybatis.reflection.JadParamNameResolver;
import com.jad.dao.utils.DaoReflectionUtil;
import com.jad.dao.utils.EntityUtils;
import com.jad.dao.utils.SqlHelper;

/**
 * @author Clinton Begin
 * @author Eduardo Macarron
 * @author Lasse Voss
 */
public class JadMapperMethod {
	

  private static Logger logger = LoggerFactory.getLogger(JadMapperMethod.class);

  private final SqlCommand command;
  private final JadMethodSignature method;
  private final SqlSession sqlSession;

  public JadMapperMethod(Class<?> mapperInterface, Method method,
			SqlSession sqlSession) {
		Configuration config=sqlSession.getConfiguration();
		this.command = new SqlCommand(config, mapperInterface, method);
	    this.method = new JadMethodSignature(config, mapperInterface, method);
	    this.sqlSession=sqlSession;
	}

  public Object execute(SqlSession sqlSession, Object[] args) {
    Object result;
    switch (command.getType()) {
      case INSERT: {
    	Object param = method.convertArgsToSqlCommandParam(args);
    	
    	int c=sqlSession.insert(command.getName(), param);
    	
    	
    	if(param!=null && (param instanceof MapperMethod.ParamMap) 
				&& ((MapperMethod.ParamMap)param).containsKey(JadAbstractInterceptor.JAD_EINTITY_PARAM)){
			Object obj = ((MapperMethod.ParamMap)param).get(JadAbstractInterceptor.JAD_EINTITY_PARAM);
			if(obj!=null && (obj instanceof EntityObject) ){
				EntityObject e=(EntityObject)obj;
				Class entityClass = e.getClass();
				EoMetaInfo ei=EntityUtils.getEoInfo(e.getClass());
				
				String keyFieldNname=ei.getKeyFieldInfo().getFieldName();
				if(((MapperMethod.ParamMap)param).containsKey(keyFieldNname) && ((MapperMethod.ParamMap)param).get(keyFieldNname)!=null ){
					System.out.println("获得id:"+((MapperMethod.ParamMap)param).get(keyFieldNname));
					ReflectionUtils.setFieldValue(obj, keyFieldNname, ((MapperMethod.ParamMap)param).get(keyFieldNname));
				}
				
			}
		}
    	
        result = rowCountResult(c);
        /*
      //20170929
    	if(args!=null && (param instanceof Map) 
    			&& sqlSession.getConfiguration().isUseGeneratedKeys()
    			&& ((Map)param).get(EntityUtils.JAD_KEY_NAME)!=null 
    			){
//    		Object[] newArgs=new Object[args.length+1];
//    		for(int i=0;i<args.length;i++){
//    			newArgs[i]=args[i];
//    		}
//    		newArgs[args.length]=((Map)param).get(EntityUtils.JAD_KEY_NAME);
//    		Object idVal = newArgs[args.length];
    		Object idVal = ((Map)param).get(EntityUtils.JAD_KEY_NAME);
    		if(idVal!=null){
    			args[args.length-1]=idVal;	
    		}
    		
//    		if(idVal!=null && (idVal instanceof Long ) ){
//    			for(Object obj:args){
//        			if(obj!=null && (obj instanceof RetId) ){
//        				RetId retId = (RetId)obj;
//        				System.out.println("主键:"+retId.getInsertedId());
//        				retId.setInsertedId((Long)idVal);
//        			}
//        		}
//    		}
    		
//    		args[args.length-1]=idVal;
    		
    		
    	}
    	*/
        break;
      }
      case UPDATE: {
        Object param = method.convertArgsToSqlCommandParam(args);
        result = rowCountResult(sqlSession.update(command.getName(), param));
        break;
      }
      case DELETE: {
        Object param = method.convertArgsToSqlCommandParam(args);
        result = rowCountResult(sqlSession.delete(command.getName(), param));
        break;
      }
      case SELECT:
        if (method.returnsVoid() && method.hasResultHandler()) {
          executeWithResultHandler(sqlSession, args);
          result = null;
        } else if (method.returnsMany()) {
          result = executeForMany(sqlSession, args);
        } else if (method.returnsMap()) {
          result = executeForMap(sqlSession, args);
        } else if (method.returnsCursor()) {
          result = executeForCursor(sqlSession, args);
        } else {
          Object param = method.convertArgsToSqlCommandParam(args);
          result = sqlSession.selectOne(command.getName(), param);
        }
        break;
      case FLUSH:
        result = sqlSession.flushStatements();
        break;
      default:
        throw new BindingException("Unknown execution method for: " + command.getName());
    }
    if (result == null && method.getReturnType().isPrimitive() && !method.returnsVoid()) {
      throw new BindingException("Mapper method '" + command.getName() 
          + " attempted to return null from a method with a primitive return type (" + method.getReturnType() + ").");
    }
    return result;
  }

  private Object rowCountResult(int rowCount) {
    final Object result;
    if (method.returnsVoid()) {
      result = null;
    } else if (Integer.class.equals(method.getReturnType()) || Integer.TYPE.equals(method.getReturnType())) {
      result = rowCount;
    } else if (Long.class.equals(method.getReturnType()) || Long.TYPE.equals(method.getReturnType())) {
      result = (long)rowCount;
    } else if (Boolean.class.equals(method.getReturnType()) || Boolean.TYPE.equals(method.getReturnType())) {
      result = rowCount > 0;
    } else {
      throw new BindingException("Mapper method '" + command.getName() + "' has an unsupported return type: " + method.getReturnType());
    }
    return result;
  }

  private void executeWithResultHandler(SqlSession sqlSession, Object[] args) {
    MappedStatement ms = sqlSession.getConfiguration().getMappedStatement(command.getName());
    if (void.class.equals(ms.getResultMaps().get(0).getType())) {
      throw new BindingException("method " + command.getName() 
          + " needs either a @ResultMap annotation, a @ResultType annotation," 
          + " or a resultType attribute in XML so a ResultHandler can be used as a parameter.");
    }
    Object param = method.convertArgsToSqlCommandParam(args);
    if (method.hasRowBounds()) {
      RowBounds rowBounds = method.extractRowBounds(args);
      sqlSession.select(command.getName(), param, rowBounds, method.extractResultHandler(args));
    } else {
      sqlSession.select(command.getName(), param, method.extractResultHandler(args));
    }
  }

  private <E> Object executeForMany(SqlSession sqlSession, Object[] args) {
    List<E> result;
    Object param = method.convertArgsToSqlCommandParam(args);
    if (method.hasRowBounds()) {
      RowBounds rowBounds = method.extractRowBounds(args);
      result = sqlSession.<E>selectList(command.getName(), param, rowBounds);
    } else {
      result = sqlSession.<E>selectList(command.getName(), param);
    }
    // issue #510 Collections & arrays support
    if (!method.getReturnType().isAssignableFrom(result.getClass())) {
      if (method.getReturnType().isArray()) {
        return convertToArray(result);
      } else {
        return convertToDeclaredCollection(sqlSession.getConfiguration(), result);
      }
    }
    return result;
  }

  private <T> Cursor<T> executeForCursor(SqlSession sqlSession, Object[] args) {
    Cursor<T> result;
    Object param = method.convertArgsToSqlCommandParam(args);
    if (method.hasRowBounds()) {
      RowBounds rowBounds = method.extractRowBounds(args);
      result = sqlSession.<T>selectCursor(command.getName(), param, rowBounds);
    } else {
      result = sqlSession.<T>selectCursor(command.getName(), param);
    }
    return result;
  }

  private <E> Object convertToDeclaredCollection(Configuration config, List<E> list) {
    Object collection = config.getObjectFactory().create(method.getReturnType());
    MetaObject metaObject = config.newMetaObject(collection);
    metaObject.addAll(list);
    return collection;
  }

  @SuppressWarnings("unchecked")
  private <E> E[] convertToArray(List<E> list) {
    E[] array = (E[]) Array.newInstance(method.getReturnType().getComponentType(), list.size());
    array = list.toArray(array);
    return array;
  }

  private <K, V> Map<K, V> executeForMap(SqlSession sqlSession, Object[] args) {
    Map<K, V> result;
    Object param = method.convertArgsToSqlCommandParam(args);
    if (method.hasRowBounds()) {
      RowBounds rowBounds = method.extractRowBounds(args);
      result = sqlSession.<K, V>selectMap(command.getName(), param, method.getMapKey(), rowBounds);
    } else {
      result = sqlSession.<K, V>selectMap(command.getName(), param, method.getMapKey());
    }
    return result;
  }

  public static class ParamMap<V> extends HashMap<String, V> {

    private static final long serialVersionUID = -2212268410512043556L;

    @Override
    public V get(Object key) {
      if (!super.containsKey(key)) {
        throw new BindingException("Parameter '" + key + "' not found. Available parameters are " + keySet());
      }
      return super.get(key);
    }

  }

  public static class SqlCommand {

    private final String name;
    private final SqlCommandType type;

    public SqlCommand(Configuration configuration, Class<?> mapperInterface, Method method) {
      String statementName = mapperInterface.getName() + "." + method.getName();
      MappedStatement ms = null;
      if (configuration.hasStatement(statementName)) {
        ms = configuration.getMappedStatement(statementName);
      } else if (!mapperInterface.equals(method.getDeclaringClass())) { // issue #35
        String parentStatementName = method.getDeclaringClass().getName() + "." + method.getName();
        if (configuration.hasStatement(parentStatementName)) {
          ms = configuration.getMappedStatement(parentStatementName);
        }
      }
      if (ms == null) {
        if(method.getAnnotation(Flush.class) != null){
          name = null;
          type = SqlCommandType.FLUSH;
        } else {
          throw new BindingException("Invalid bound statement (not found): " + statementName);
        }
      } else {
        name = ms.getId();
        type = ms.getSqlCommandType();
        if (type == SqlCommandType.UNKNOWN) {
          throw new BindingException("Unknown execution method for: " + name);
        }
      }
    }

    public String getName() {
      return name;
    }

    public SqlCommandType getType() {
      return type;
    }
  }
  

	/**
	 * 为了兼容jpa
	 * @param entity
	 * @return
	 */
	public <S> S save(S entity){
		Object param = method.convertArgsToSqlCommandParam(new Object[]{entity});
		sqlSession.insert(command.getName(), param);
		return entity;
	}
	
	
	
	
	
	public static class JadMethodSignature {

	    private final boolean returnsMany;
	    private final boolean returnsMap;
	    private final boolean returnsVoid;
	    private final boolean returnsCursor;
	    private final Class<?> returnType;
	    private final String mapKey;
	    private final Integer resultHandlerIndex;
	    private final Integer rowBoundsIndex;
	    private JadParamNameResolver paramNameResolver;

	    public JadMethodSignature(Configuration configuration, Class<?> mapperInterface, Method method) {
	      Type resolvedReturnType = TypeParameterResolver.resolveReturnType(method, mapperInterface);
	      if (resolvedReturnType instanceof Class<?>) {
	        this.returnType = (Class<?>) resolvedReturnType;
	      } else if (resolvedReturnType instanceof ParameterizedType) {
	        this.returnType = (Class<?>) ((ParameterizedType) resolvedReturnType).getRawType();
	      } else {
	        this.returnType = method.getReturnType();
	      }
	      this.returnsVoid = void.class.equals(this.returnType);
	      this.returnsMany = (configuration.getObjectFactory().isCollection(this.returnType) || this.returnType.isArray());
	      this.returnsCursor = Cursor.class.equals(this.returnType);
	      this.mapKey = getMapKey(method);
	      this.returnsMap = (this.mapKey != null);
	      this.rowBoundsIndex = getUniqueParamIndex(method, RowBounds.class);
	      this.resultHandlerIndex = getUniqueParamIndex(method, ResultHandler.class);
	      
	      this.paramNameResolver = new JadParamNameResolver(configuration, method);  	  
	      
	    }

	    public Object convertArgsToSqlCommandParam(Object[] args) {
	      return paramNameResolver.getNamedParams(args);
	    }
	    

	    public boolean hasRowBounds() {
	      return rowBoundsIndex != null;
	    }

	    public RowBounds extractRowBounds(Object[] args) {
	      return hasRowBounds() ? (RowBounds) args[rowBoundsIndex] : null;
	    }

	    public boolean hasResultHandler() {
	      return resultHandlerIndex != null;
	    }

	    public ResultHandler extractResultHandler(Object[] args) {
	      return hasResultHandler() ? (ResultHandler) args[resultHandlerIndex] : null;
	    }

	    public String getMapKey() {
	      return mapKey;
	    }

	    public Class<?> getReturnType() {
	      return returnType;
	    }

	    public boolean returnsMany() {
	      return returnsMany;
	    }

	    public boolean returnsMap() {
	      return returnsMap;
	    }

	    public boolean returnsVoid() {
	      return returnsVoid;
	    }

	    public boolean returnsCursor() {
	      return returnsCursor;
	    }

	    private Integer getUniqueParamIndex(Method method, Class<?> paramType) {
	      Integer index = null;
	      final Class<?>[] argTypes = method.getParameterTypes();
	      for (int i = 0; i < argTypes.length; i++) {
	        if (paramType.isAssignableFrom(argTypes[i])) {
	          if (index == null) {
	            index = i;
	          } else {
	            throw new BindingException(method.getName() + " cannot have multiple " + paramType.getSimpleName() + " parameters");
	          }
	        }
	      }
	      return index;
	    }

	    private String getMapKey(Method method) {
	      String mapKey = null;
	      if (Map.class.isAssignableFrom(method.getReturnType())) {
	        final MapKey mapKeyAnnotation = method.getAnnotation(MapKey.class);
	        if (mapKeyAnnotation != null) {
	          mapKey = mapKeyAnnotation.value();
	        }
	      }
	      return mapKey;
	    }
	  }


}
