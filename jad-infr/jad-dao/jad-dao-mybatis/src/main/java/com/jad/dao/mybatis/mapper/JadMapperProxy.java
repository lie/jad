package com.jad.dao.mybatis.mapper;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.reflection.ExceptionUtil;
import org.apache.ibatis.session.SqlSession;

/**
 * @author Clinton Begin
 * @author Eduardo Macarron
 */
public class JadMapperProxy<T> implements InvocationHandler, Serializable {

  private static final long serialVersionUID = -6424540398559729838L;
  private final SqlSession sqlSession;
  private final Class<T> mapperInterface;
  private final Map<Method, JadMapperMethod> methodCache;

  public JadMapperProxy(SqlSession sqlSession, Class<T> mapperInterface, Map<Method, JadMapperMethod> methodCache) {
    this.sqlSession = sqlSession;
    this.mapperInterface = mapperInterface;
    this.methodCache = methodCache;
  }
	@Override
	  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Class declarClass=method.getDeclaringClass();
		if (Object.class.equals(declarClass)) {
	      try {
	        return method.invoke(this, args);
	      } catch (Throwable t) {
	        throw ExceptionUtil.unwrapThrowable(t);
	      }
	    }
	    final JadMapperMethod mapperMethod = cachedMapperMethod(method);
	    Object res =mapperMethod.execute(sqlSession, args);
	    
	    return res;
	    
	  }
	

	  private JadMapperMethod cachedMapperMethod(Method method) {
		 JadMapperMethod mapperMethod = (JadMapperMethod)methodCache.get(method);
	    if (mapperMethod == null) {
	      mapperMethod = new JadMapperMethod(mapperInterface, method, sqlSession);
	      methodCache.put(method, mapperMethod);
	    }
	    return mapperMethod;
	  }
	

}
