package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;

import javax.persistence.OrderBy;

import com.jad.commons.utils.StringUtils;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.entity.EoFieldInfo;

public class OrderByParser extends AbstractEoFieldAnnotationParser<OrderBy>{


	@Override
	public <EO extends EntityObject> boolean parseAnnotationEoField(
			EoFieldInfo<EO> eoFieldInfo, Field field, OrderBy annotation) {
		
		if(StringUtils.isNotBlank(annotation.value())){
			eoFieldInfo.setOrderBy(annotation.value());
			return true;
		}else{
			return false;
		}
	}

	
}
