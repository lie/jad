package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.entity.EoFieldInfo;
import com.jad.dao.enums.JavaType;
import com.jad.dao.exception.JadEntityParseException;

public class TemporalParser extends AbstractEoFieldAnnotationParser<Temporal>{


	@Override
	public <EO extends EntityObject> boolean parseAnnotationEoField(
			EoFieldInfo<EO> eoFieldInfo, Field field, Temporal temporal) {
		
		String type=JavaType.getJavaType(eoFieldInfo.getFieldType().getName()).getType();
		
		if(!JavaType.utilDate.getType().equalsIgnoreCase(type)
				&& !JavaType.Calendar.getType().equalsIgnoreCase(type) ){
				
			throw new JadEntityParseException(
					String.format("实体类%s的属性%s上不能使用Temporal注解",
							eoFieldInfo.getEoInfo().getMetaClass().getName(),
							eoFieldInfo.getFieldName()));
		}
			
		eoFieldInfo.setTemporalType(temporal.value()==null?TemporalType.TIMESTAMP:temporal.value());
		
		return true;
	}


}
