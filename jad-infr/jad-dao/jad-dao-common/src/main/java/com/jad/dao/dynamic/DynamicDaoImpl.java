package com.jad.dao.dynamic;

import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;

@JadDao("dynamicCurdDao")
@SuppressWarnings("rawtypes")
public class DynamicDaoImpl extends AbstractJadEntityDao implements DynamicDao {
}
