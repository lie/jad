package com.jad.dao.entity;

import java.lang.reflect.Field;

import javax.persistence.TemporalType;

/**
 * 对像属性信息
 * @author hechuan
 *
 */
public class ObjectFieldInfo {

	protected Field field;
	
	protected Class fieldType;
	
	protected String fieldName;
	
	protected int length=255;
	protected int precision=0;
	protected int scale=0;
	
	/**
	 * 如果字段为java.util.Date 或 java.util.Calendar类型，
	 * 且用  Temporal 注解标注了此字段 ,这里保存Temporal注解的值
	 */
	protected TemporalType temporalType;
	
	public ObjectFieldInfo(Field field){
		this.field=field;
		this.fieldType=this.field.getType();
		this.fieldName=field.getName();
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	public Class getFieldType() {
		return fieldType;
	}

	public void setFieldType(Class fieldType) {
		this.fieldType = fieldType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public int getScale() {
		return scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}

	public TemporalType getTemporalType() {
		return temporalType;
	}

	public void setTemporalType(TemporalType temporalType) {
		this.temporalType = temporalType;
	}



	
	
	
}
