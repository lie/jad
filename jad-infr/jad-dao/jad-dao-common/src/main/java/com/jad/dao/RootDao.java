package com.jad.dao;

import java.util.List;

import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.Page;
import com.jad.dao.annotation.Param;

public interface RootDao {



	/**
	 * 插入  20170929
	 * @param sql jadKeyParam
	 * @param params
	 * @return
	 */
//	int insertSql(
//			@Param("jadSql")String sql,
//			@Param("jadListParam")List<?> params,
//			@Param("jadKeyParam")Object retId
//			) ;
	
	<E extends EntityObject>int insert(@Param("jadEintityParam")E e);
	
//	int insertSql(
//			@Param("jadSql")String sql,
//			@Param("jadListParam")List<?> params,
//			@Param("jadKeyParam")Object retId
//			) ;
	
	int executeSql(
			@Param("jadSql")String sql,
			@Param("jadListParam")List<?> params) ;
	
	<E>List<E> findBySql(
			@Param("jadPage")Page<?> page,
			@Param("jadSql")String sql,
			@Param("jadListParam")List<?> params,
			@Param("jadResultType")Class<E>entityClass);
	
}

