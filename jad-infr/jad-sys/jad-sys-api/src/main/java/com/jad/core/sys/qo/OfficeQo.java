package com.jad.core.sys.qo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.qo.TreeQo;

public class OfficeQo extends TreeQo<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@CURD(label="机构类型")
	private String type; 	// 机构类型（1：公司；2：部门；3：小组）
	
	@CURD(label="机构等级")
	private String grade; 	// 机构等级（1：一级；2：二级；3：三级；4：四级）

	public OfficeQo() {
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

}
