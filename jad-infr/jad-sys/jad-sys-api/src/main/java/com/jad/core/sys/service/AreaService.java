/**
 */
package com.jad.core.sys.service;

import com.jad.commons.service.TreeService;
import com.jad.core.sys.vo.AreaVo;

/**
 * 区域Service
 * @author ThinkGem
 * @version 2014-05-16
 */
public interface AreaService extends TreeService<AreaVo,String> {
	

	
}
