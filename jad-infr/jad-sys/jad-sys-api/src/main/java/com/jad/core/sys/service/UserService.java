package com.jad.core.sys.service;

import java.util.List;

import com.jad.commons.service.CurdService;
import com.jad.core.sys.vo.UserVo;

public interface UserService extends CurdService<UserVo,String>{
	
	public UserVo findByLoginName(String loginName);
	
	/**
	 * 加密密码
	 * @param plainPassword
	 * @return
	 */
	public String entryptPassword(String plainPassword);

	/**
	 * 校验密码
	 * @param plainPassword
	 * @param password
	 * @return
	 */
	public boolean validatePassword(String plainPassword, String password);
	
	
	/**
	 * 查询拥有指定角色的用户
	 * @param roleId
	 * @return
	 */
	public List<UserVo>findByRoleId(String roleId);
	
	public int updatePasswordById(String id,String password);
	
	/**
	 * 移除用户角色
	 * @param userId
	 * @param roleId
	 * @return
	 */
	public int deleteUserRole(String userId,String roleId);
	
	/**
	 * 分配角色
	 * @param userId
	 * @param roleId
	 * @return
	 */
	public UserVo assignUserRole(String userId,String roleId);
	

}
