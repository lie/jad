package com.jad.core.sys.vo;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.TreeVo;
import com.jad.core.sys.util.MenuUtil;

public class MenuVo extends TreeVo<MenuVo,String>  {

	private static final long serialVersionUID = 1L;
	
	private MenuVo parent;	// 父级菜单
	
	@CURD(label="链接")
	private String href; 	// 链接
	
	@CURD(label="目标")
	private String target; 	// 目标（ mainFrame、_blank、_self、_parent、_top）
	
	@CURD(label="图标")
	private String icon; 	// 图标
	
	@CURD(label="是否显示")
	private String isShow; 	// 是否在菜单中显示（1：显示；0：不显示）
	
	@CURD(label="权限标识")
	private String permission; // 权限标识
	
//	@CURD(label="用户id")
//	private String userId;
	
	public String getDefRootId(){
		return MenuUtil.getRootId();
	}
	
	public MenuVo(){
		super();
	}
	
	public MenuVo(String id){
		super(id);
	}
	
	@JsonBackReference
	@NotNull
	public MenuVo getParent() {
		return parent;
	}

	public void setParent(MenuVo parent) {
		this.parent = parent;
	}


	@Length(min=0, max=2000)
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	@Length(min=0, max=20)
	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
	
	@Length(min=0, max=100)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@Length(min=1, max=1)
	public String getIsShow() {
		return isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

	@Length(min=0, max=200)
	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	
//	public String getUserId() {
//		return userId;
//	}
//
//	public void setUserId(String userId) {
//		this.userId = userId;
//	}


}
