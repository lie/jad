/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.sys.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.commons.util.QoUtil;
import com.jad.commons.utils.Validate;
import com.jad.core.sys.dao.DictDao;
import com.jad.core.sys.entity.DictEo;
import com.jad.core.sys.qo.DictQo;
import com.jad.core.sys.service.DictService;
import com.jad.core.sys.vo.DictVo;
import com.jad.dao.utils.EntityUtils;

/**
 * 字典Service
 * @author ThinkGem
 * @version 2014-05-16
 */
@Service("dictService")
@Transactional(readOnly = true)
public class DictServiceImpl extends AbstractServiceImpl<DictEo, String,DictVo> implements DictService{
	
	@Autowired
	private DictDao dictDao;
	
	/**
	 * 查询字段类型列表
	 * @return
	 */
	public List<String> findTypeList(){
		return dictDao.findTypeList();
	}
	
	
	public String getDictValue(String label, String type, String defaultValue){

		Validate.allNotBlank(label,type);
		
		DictQo qo = QoUtil.newNormalDataQo(DictQo.class);
		qo.setType(type);
		qo.setLabel(label);
		
		List<DictEo> eoList = dictDao.findByQo(qo);
		if(CollectionUtils.isNotEmpty(eoList)){
			if(eoList.get(0) !=null && StringUtils.isNotBlank(eoList.get(0).getValue())){
				return eoList.get(0).getValue();
			}
		}
		
		return defaultValue;
	}
	
	public String findDictLabel(String value, String type, String defaultLabel){
		
		Validate.allNotBlank(value,type);
		
		DictQo qo = QoUtil.newNormalDataQo(DictQo.class);
		qo.setType(type);
		qo.setValue(value);
		
		List<DictEo> eoList = dictDao.findByQo(qo);
		if(CollectionUtils.isNotEmpty(eoList)){
			if(eoList.get(0) !=null && StringUtils.isNotBlank(eoList.get(0).getLabel())){
				return eoList.get(0).getLabel();
			}
		}
		
		return defaultLabel;
	}

	public List<DictVo> findByType(String type){
		
		Validate.notBlank(type);
		
		DictQo qo = QoUtil.newNormalDataQo(DictQo.class);
		qo.setType(type);
		
		return EntityUtils.copyToVoList(dictDao.findByQo(qo), DictVo.class);
		
	}



}
