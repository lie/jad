package com.jad.core.sys.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jad.core.sys.entity.MenuEo;
import com.jad.test.dao.BaseDaoTestCase;

public class MenuDaoTest extends BaseDaoTestCase {

	public static final Logger logger = LoggerFactory.getLogger(MenuDaoTest.class);

	private MenuDao menuDao = null;

	@Test
	public void mainTest() throws Exception {
		 findByUserId();
		// findByParentIdsLike();
//		updateParentIds();
//		updateSort();
	}

	// @Test
	public void findByParentIdsLike() throws Exception {
		String parentIds = "0,1,2,13,14,";
//		List<MenuEo> eoList = menuDao.findByParentIdsLike(parentIds);
//		System.out.println("总数:" + eoList.size());

	}

	// @Test
	public void findByUserId() throws Exception {
		String userId = "1";
		List<MenuEo> eoList = menuDao.findByUserId(userId);
		System.out.println("总数:" + eoList.size());
	}

	// @Test
	public void updateParentIds() throws Exception {
		String id = "10";
		String parentId = "3";
		String parentIds = "0,1,2,3,";
		int i = menuDao.updateParentIds(id, parentId, parentIds);
		Assert.isTrue(i == 1);

	}

	// @Test
	public void updateSort() throws Exception {
		String id = "10";
		Integer sort = 60;
		int i = menuDao.updateSort(id, sort);
		Assert.isTrue(i == 1);
	}

	@BeforeTest
	public void beforeDaoTest() throws Exception {
		menuDao = getBean("menuDao");
	}
}
