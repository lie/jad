package com.jad.commons.entity;

import java.io.Serializable;

import javax.persistence.Column;

public class UserDataEo<ID extends Serializable> extends DataEo<ID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String createBy;	// 创建者
	
	protected String updateBy;	// 更新者
	
	protected String remarks;	// 备注
	
	public UserDataEo(){
		
	}
	public UserDataEo(ID id){
		super(id);
	}
	


	

	
	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	@Column(name="create_by")
	public String getCreateBy() {
		return createBy;
	}
	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	@Column(name="update_by")
	public String getUpdateBy() {
		return updateBy;
	}
	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}


}



