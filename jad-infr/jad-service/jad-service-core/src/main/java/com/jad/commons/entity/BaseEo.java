package com.jad.commons.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.jad.commons.vo.EntityObject;

@MappedSuperclass
public class BaseEo<ID extends Serializable> implements EntityObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 */
	protected ID id;

	
	protected Date createDate;	// 创建日期
	protected Date updateDate;	// 更新日期
	
	protected String createBy;	// 创建者
	protected String updateBy;	// 更新者
	
	protected String remarks;	// 备注
	
	protected String delFlag; 	// 删除标记（0：正常；1：删除；2：审核）
	
	public BaseEo(){
		
	}
	
	public BaseEo(ID id){
		this.id=id;
	}
	
	
	@Id
	@Column(name="id")
	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

	
	@Column(name="create_date",updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	@Column(name="update_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	@Column(name="del_flag")
	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}


	@Column(name="remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	@Column(name="create_by")
	public String getCreateBy() {
		return createBy;
	}
	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	@Column(name="update_by")
	public String getUpdateBy() {
		return updateBy;
	}
	
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this,obj);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	

}
