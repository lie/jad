package com.jad.commons.service.impl;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.jad.commons.context.Constants;
import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.service.AbstractPreService;
import com.jad.commons.service.CurdService;
import com.jad.commons.service.ServiceException;
import com.jad.commons.util.QoUtil;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.DataVo;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.JadBaseVo;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.QueryObject;
import com.jad.commons.vo.ValueObject;
import com.jad.dao.JadEntityDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.VoMetaInfo;
import com.jad.dao.utils.EntityUtils;

@Transactional(readOnly = true)
public abstract class AbstractCurdServiceImpl<EO extends EntityObject, ID extends Serializable,VO extends JadBaseVo<ID>>
	extends AbstractPreService<EO,ID> implements InitializingBean,ApplicationContextAware,CurdService<VO,ID>{
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractCurdServiceImpl.class);

	protected JadEntityDao<EO,ID> dao;
	
	protected EoMetaInfo<EO>eoMetaInfo;	
	
	protected VoMetaInfo<VO>voMetaInfo;
	
	protected ApplicationContext applicationContext;
	
	/**
	 * 默认自动跟据Eo或者Vo的名称自动加载，子类可覆盖 
	 * @return
	 */
	protected JadEntityDao<EO,ID> getDao(){
		if(dao!=null){
			return dao;
		}
		
		dao=autoGetDao(eoMetaInfo.getMetaClass());
		
		if(dao!=null){
			return dao;
		}
		dao=autoGetDao(voMetaInfo.getMetaClass());
		
		if(dao==null){
			throw new ServiceException("service中无法自动获取dao,service:"+this.getClass().getName());
		}
		
		return dao;
	}
	
	/**
	 * 通过eo或vo的名称自动从srping中加载dao后缀的bean
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected JadEntityDao<EO,ID>autoGetDao(Class<?>clazz){
		
		String name=clazz.getSimpleName();
		
		String suffix=null;
		if(EntityObject.class.isAssignableFrom(clazz)){
			suffix="Eo";
		}else if(ValueObject.class.isAssignableFrom(clazz)){
			suffix="Vo";
		}else{
			logger.warn("无法自动获得Dao,因为%s类型末知",clazz.getName());
			return null;
		}
		
		String daoName=null;
		if(name.endsWith(suffix)){
			daoName=StringUtils.uncapitalize(name.substring(0,name.length()-suffix.length()));
		}else{
			daoName=StringUtils.uncapitalize(name);
		}
		
		daoName = daoName+"Dao";
		
		JadEntityDao<EO,ID>dao=null;
		
		try {
			if(applicationContext.containsBean(daoName)){
				
				Object bean = applicationContext.getBean(daoName);
				
				if(bean !=null && bean instanceof JadEntityDao){
					dao = (JadEntityDao<EO,ID>)bean;
				}
			}
		} catch (Exception e) {
			logger.warn(String.format("无法跟据%s:%s自动加载dao",suffix, name));
		}
		
		if(dao!=null){
			this.dao=dao;
		}
		
		return dao;
	}
	
	protected EO newEo(){
		try {
			
			return eoMetaInfo.getMetaClass().newInstance();
			
		} catch (Exception e) {
			
			throw new ServiceException(e);
			
		} 
		
	}
	
	protected VO newVo(){
		
		try {
			
			return voMetaInfo.getMetaClass().newInstance();
			
		} catch (Exception e) {
			
			throw new ServiceException(e);
			
		} 
		
		
	}
	
	protected EO toEo(VO vo){
		if(vo==null){
			return null;
		}
		return EntityUtils.copyToEo(vo, eoMetaInfo.getMetaClass());
	}
	
	protected VO toVo(EO eo){
		if(eo==null){
			return null;
		}
		return EntityUtils.copyToVo(eo, voMetaInfo.getMetaClass());
	}
	
	
	
	
	
	@Override
	public VO findById(ID id) {
		Assert.notNull(id);
		EO eo = getDao().findById(id);
		if(eo==null){
			return null;
		}
		return toVo(eo);
	}
	
	@Override
	public List<VO> findByIdList(List<ID> idList){
		Assert.notEmpty(idList);
		return findByIdList(idList,null);
	}
	
	@Override
	public List<VO> findByIdList(List<ID> idList,String orderBy){
		Assert.notEmpty(idList);
		return EntityUtils.copyToVoList(getDao().findByIdList(idList,orderBy), voMetaInfo.getMetaClass());
	}

	@Override
	public <QO extends QueryObject> List<VO> findList(QO qo) {
		return findList(qo,null);
	}
	
	@Override
	public <QO extends QueryObject> List<VO> findList(QO qo,String orderBy) {
		Assert.notNull(qo);
		List<EO>list=getDao().findByQo(QoUtil.wrapperNormalDataQo(qo),orderBy);
		
		return EntityUtils.copyToVoList(list, voMetaInfo.getMetaClass());
		
	}
	
	@Override
	public <QO extends QueryObject> Page<VO> findPage(Page<VO> page, QO qo,String orderBy) {
		
		Assert.notNull(page);
		Assert.notNull(qo);
		
		Page<EO>dataPage=getDao().findPageByQo(page, QoUtil.wrapperNormalDataQo(qo),orderBy);
		
		Page<VO>voPage=new Page<VO>(dataPage.getPageNo(),dataPage.getPageSize(),dataPage.getCount());
		
		voPage.setList(EntityUtils.copyToVoList(dataPage.getList(), voMetaInfo.getMetaClass()));
		
		return voPage;
	}

	@Override
	public <QO extends QueryObject> Page<VO> findPage(Page<VO> page, QO qo) {
		
		return findPage(page,qo,null);
		
	}
	
	
	/**
	 * 增加 
	 * @param vo
	 */
	@Override
	@Transactional(readOnly = false)
	public void add(VO vo){
		Assert.notNull(vo);
		preInsert(vo,eoMetaInfo);
		getDao().add(toEo(vo));
	}
	
	
	
	/**
	 * 更新
	 * @param vo
	 */
	@Override
	@Transactional(readOnly = false)
	public void update(VO vo){
		Assert.notNull(vo);
		preUpdate(vo);
		getDao().updateById(toEo(vo), getIdFromVo(vo));
	}
	
	protected ID getIdFromVo(VO vo){
		ID id=(ID)ReflectionUtils.getFieldValue(vo, this.getEoMetaInfo().getKeyFieldInfo().getFieldName());
		return id;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = false)
	public void delete(VO vo) {
		
		Assert.notNull(vo);
		
//		getDao().deleteById(getId(vo));//物理删除
		
		ID id = getIdFromVo(vo);
		
		if(id!=null){

			((DataVo)vo).setDelFlag(Constants.DEL_FLAG_DELETE);
			this.update(vo);//逻辑删除
			
		}else{
			VoMetaInfo<VO>voi=EntityUtils.getVoInfo((Class<VO>)vo.getClass());
			logger.warn(String.format("无法删除%s，末知id", voi.getObjName()) );
		}
		
		
		
	}
	
	
	
	public void afterPropertiesSet() throws Exception{
		initEoMetaInfo();
		initVoMetaInfo();
		getDao();//加载dao
	}
	
	
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException{
		this.applicationContext=applicationContext;
	}
	
	
	@SuppressWarnings("unchecked")
	protected void initEoMetaInfo(){
		if(eoMetaInfo==null){
			Class<EO> entityClass=ReflectionUtils.getClassGenricType(getClass(), 0);
			if(!EntityObject.class.isAssignableFrom(entityClass)){
				throw new ServiceException(String.format("service类%s初始化错误,T必须为RootEo类或子类", this.getClass().getName()));
			}
			eoMetaInfo=EntityUtils.getEoInfo(entityClass);
		}
		
	}
	

	
	@SuppressWarnings("unchecked")
	protected void initVoMetaInfo(){
		if(voMetaInfo==null){
			Class<VO>voClass=ReflectionUtils.getClassGenricType(getClass(), 2);
			if(!ValueObject.class.isAssignableFrom(voClass)){
				throw new ServiceException(String.format("service类%s初始化错误,VO必须为RootVo类或子类", this.getClass().getName()));
			}
			voMetaInfo=EntityUtils.getVoInfo(voClass);
		}
		
	}

	public EoMetaInfo<EO> getEoMetaInfo() {
		return eoMetaInfo;
	}

	public void setEoMetaInfo(EoMetaInfo<EO> eoMetaInfo) {
		this.eoMetaInfo = eoMetaInfo;
	}

	public VoMetaInfo<VO> getVoMetaInfo() {
		return voMetaInfo;
	}

	public void setVoMetaInfo(VoMetaInfo<VO> voMetaInfo) {
		this.voMetaInfo = voMetaInfo;
	}

	public void setDao(JadEntityDao<EO, ID> dao) {
		this.dao = dao;
	}

	
	
	
	
}
