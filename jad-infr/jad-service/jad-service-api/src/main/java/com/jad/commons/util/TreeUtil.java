package com.jad.commons.util;

import java.io.Serializable;
import java.util.List;

import com.jad.commons.vo.TreeVo;

public class TreeUtil {

	/**
	 * 树节点排序
	 * @param list 排 序后的列表
	 * @param sourcelist 源列表
	 * @param parentId 父id
	 */
	public static <VO extends TreeVo<VO,ID> ,ID extends Serializable> void sortList(List<VO> list, List<VO> sourcelist, ID parentId ){
		sortList(list,sourcelist,parentId,true);
	}

	
	/**
	 * 树节点排序
	 * @param list 排 序后的列表
	 * @param sourcelist 源列表
	 * @param parentId 父id
	 * @param cascade 是否递归
	 */
	public static <VO extends TreeVo<VO,ID> ,ID extends Serializable> void sortList(List<VO> list, List<VO> sourcelist, ID parentId, boolean cascade){
		for (int i=0; i<sourcelist.size(); i++){
			VO vo = sourcelist.get(i);
			if (vo.getParent()!=null && vo.getParent().getId()!=null
					&& vo.getParent().getId().equals(parentId)){
				list.add(vo);
				if (cascade){
					// 判断是否还有子节点, 有则继续获取子节点
					for (int j=0; j<sourcelist.size(); j++){
						VO child = sourcelist.get(j);
						if (child.getParent()!=null && child.getParent().getId()!=null
								&& child.getParent().getId().equals(vo.getId())){
							sortList(list, sourcelist, vo.getId(), true);
							break;
						}
					}
				}
			}
		}
	}
	
	
}
