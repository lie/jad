package com.jad.web.mvc.api;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import com.jad.commons.encrypt.Encodes;
import com.jad.commons.security.shiro.SystemAuthorizingRealm;
import com.jad.commons.security.shiro.UsernamePasswordToken;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.web.api.MsgRespCode;

public class ApiAuthorizingRealm extends SystemAuthorizingRealm{
	
//	@Autowired
//	private UserLoginService userLoginService;
	
	/**
	 * 认证回调函数, 登录时调用
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) {
		if(authcToken instanceof ApiToken ){
			
			ApiToken apiToken = (ApiToken)authcToken;
			
//			if(StringUtils.isBlank(apiToken.getToken())){
//				throw new ApiHandlerException(MsgRespCode.NO_LOGIN);
//			}
			
//			String tk=apiToken.getToken().trim();
//			boolean check = userLoginService.checkToken(tk);
//			if(!check){
//				throw new ApiHandlerException(MsgRespCode.NO_LOGIN);
//			}
//			String pwd = PasswordUtil.entryptPassword(apiToken.getToken().trim());
			String pwd="";
			
			byte[] salt = Encodes.decodeHex(pwd.substring(0,16));
			return new ApiAuthenticationInfo(new ApiPrincipal(apiToken.getToken().trim() ), 
					pwd.substring(16),ByteSource.Util.bytes(salt),getName());
			
		}else{
			return super.doGetAuthenticationInfo(authcToken);
		}
	}
	
	/**
	 * 
	 * 授权回调
	 * 
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		Object p = getAvailablePrincipal(principals);
		if(p instanceof ApiPrincipal){
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			info.addStringPermission("api");
			return info;
		}else {
			return super.doGetAuthorizationInfo(principals);
		}
	}
	
	public boolean supports(AuthenticationToken token){
		 return token != null 
				 && (ApiToken.class.isAssignableFrom(token.getClass()) 
				 || UsernamePasswordToken.class.isAssignableFrom(token.getClass()) ) ;
	 }


}
