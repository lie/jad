package com.jad.web.mvc.api;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.servlet.AdviceFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.utils.StringUtils;
import com.jad.commons.web.api.util.HttpRequestUtil;

public class ApiFilter extends AdviceFilter {

	private static final Logger logger = LoggerFactory.getLogger(ApiFilter.class);

	protected boolean preHandle(ServletRequest request, ServletResponse response)throws Exception {

//		读取请求数据
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		String jsonBody = IOUtils.toString(servletRequest.getInputStream());
		servletRequest.setAttribute(HttpRequestUtil.JSON_REQUEST_BODY,jsonBody);

		logger.debug("收到请求:" + jsonBody);
		
		// TODO 验签

		Subject subject = SecurityUtils.getSubject();
		String token = getToken(jsonBody);
		
		String trxCode = getTrxCode(jsonBody);
		
		ApiToken apiToken = new ApiToken(token);
		
		if(needLogin(trxCode)){
			subject.login(apiToken); // 登录
		}
		
		if(trxCode!=null && !"".equals(trxCode)){
			servletRequest.setAttribute(HttpRequestUtil.DATA_NEED_UNPACK,HttpRequestUtil.DATA_NEED_UNPACK);
		}
		return true;
	}
	
	public static boolean needUnpack(String jsonBody){
		String trxCode = getTrxCode(jsonBody);
		if(trxCode!=null && !"".equals(trxCode)){
			return true;
		}
		return false;
	}
	
	private boolean needLogin(String trxCode){
		if("login".equals(trxCode) 
				|| "logout".equals(trxCode) 
				|| "register".equals(trxCode) 
				|| "getLoginCheckCode".equals(trxCode) ){
			return false;
		}
		return true;
	}
	

	protected void postHandle(ServletRequest request, ServletResponse response)throws Exception {
		request.removeAttribute(HttpRequestUtil.JSON_REQUEST_BODY);
		request.removeAttribute(HttpRequestUtil.DATA_NEED_UNPACK);
		request.removeAttribute(HttpRequestUtil.JSON_REQUEST_HEAD);
	}
	
    
    protected void cleanup(ServletRequest request, ServletResponse response, Exception existing)
            throws ServletException, IOException {
    	 Exception exception = existing;
         try {
             afterCompletion(request, response, exception);
//             if (logger.isTraceEnabled()) {
//            	 logger.trace("Successfully invoked afterCompletion method.");
//             }
         } catch (Exception e) {
             if (exception == null) {
                 exception = e;
             } else {
            	 logger.debug("afterCompletion implementation threw an exception.  This will be ignored to " +
                         "allow the original source exception to be propagated.", e);
             }
         }
         if (exception != null) {
        	 ApiHandlerExceptionResolver.delException((HttpServletRequest)request, (HttpServletResponse)response, exception);
         }
    }
	

    public static String getToken(String jsonMsg) {
		if (StringUtils.isBlank(jsonMsg))return null;
		Pattern p = Pattern.compile("\"token\":\"(.+?)\"");
		Matcher m = p.matcher(jsonMsg);
		if (m.find())return m.group(1);
		return null;
	}
	public static String getTrxCode(String jsonMsg) {
		if (StringUtils.isBlank(jsonMsg))return null;
		Pattern p = Pattern.compile("\"trxCode\":\"(.+?)\"");
		Matcher m = p.matcher(jsonMsg);
		if (m.find())return m.group(1);
		return null;
	}
	
	/*
	public static String getSignMsg(String jsonMsg) {
		if (StringUtils.isBlank(jsonMsg))
			return null;

		Pattern p = Pattern.compile("\"signMsg\":\"(.+?)\"");
		Matcher m = p.matcher(jsonMsg);

		if (m.find())
			return m.group(1);

		return null;
	}

	public static String removeSignMsg(String jsonMsg) {
		if (StringUtils.isBlank(jsonMsg))
			return null;

		Pattern p1 = Pattern.compile("\"signMsg\":\"(.+?)\",");
		Matcher m1 = p1.matcher(jsonMsg);
		if (m1.find())
			return jsonMsg.replace(m1.group(), "");

		Pattern p2 = Pattern.compile(",\"signMsg\":\"(.+?)\"");
		Matcher m2 = p2.matcher(jsonMsg);
		if (m2.find())
			return jsonMsg.replace(m2.group(), "");

		Pattern p3 = Pattern.compile("\"signMsg\":\"(.+?)\"");
		Matcher m3 = p3.matcher(jsonMsg);
		if (m3.find()) {
			return jsonMsg.replace(m3.group(), "");
		}

		return jsonMsg;
	}
	*/
	
	
	
}



