package com.jad.web.mvc.api;

import com.jad.commons.web.api.MsgRespCode;



public class ApiHandlerException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private MsgRespCode code;
	
	public ApiHandlerException(MsgRespCode code) {
		this.code=code;
	}
	
	
	public ApiHandlerException(MsgRespCode code,String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.code=code;
	}





	public ApiHandlerException(MsgRespCode code,String message, Throwable cause) {
		super(message, cause);
		this.code=code;
	}





	public ApiHandlerException(MsgRespCode code,String message) {
		super(message);
		this.code=code;
	}





	public ApiHandlerException(MsgRespCode code,Throwable cause) {
		super(cause);
		this.code=code;
	}





	public MsgRespCode getCode() {
		return code;
	}

	public void setCode(MsgRespCode code) {
		this.code = code;
	}
	

}
