package com.jad.commons.validator.annotation;

import com.jad.commons.validator.MsgValidateException;

/**
 * <li>报文校验异常</li>
 */
public class MsgAnnValidateException extends MsgValidateException {

	public MsgAnnValidateException() {
	}

	public MsgAnnValidateException(String message) {
		super(message);
	}

	public MsgAnnValidateException(String message, Throwable cause) {
		super(message, cause);
	}

	public MsgAnnValidateException(Throwable cause) {
		super(cause);
	}

}
