package com.jad.commons.web.api.req;

import java.io.Serializable;

import com.jad.commons.validator.annotation.MsgAnnotationValidator;
import com.jad.commons.validator.annotation.MsgValidable;

public class MsgReq<D extends MsgReqBody> implements Serializable,MsgValidable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String trxCode;//交易码	String	否		接口编号
	private String reqTime;//请求时间	String			YYYYMMDDHHMMSS
	private String ver;//接口版本	String		1	1第一个版本
	private String deviceInfo;//	设备信息	String			客户系统标识符。
	private String dataType;//	报文类型	String		1	1表示json格式2表示xml(目前只支持json)
	private String sn;//流水号	String	否		请求流水，为方便后台日志跟踪，请尽量保持唯一。生成流水号的参考方式是：用设备串号加系统当前时间的毫秒数。
	private String token;//登录token	String			用户登录后可获取
	
	private D data;
	
	private String signMsg;
	
	public MsgReq(){
		
	}
	public MsgReq(String trxCode){
		this.trxCode=trxCode;
	}

	
	public void validate(){
		MsgAnnotationValidator.getInstance().validate(this);
	}
	
//	public InnReqHead getHead() {
//		return head;
//	}
//
//	public void setHead(InnReqHead head) {
//		this.head = head;
//	}


	public String getSignMsg() {
		return signMsg;
	}

	public D getData() {
		return data;
	}
	public void setData(D data) {
		this.data = data;
	}
	public void setSignMsg(String signMsg) {
		this.signMsg = signMsg;
	}

	public String getTrxCode() {
		return trxCode;
	}

	public void setTrxCode(String trxCode) {
		this.trxCode = trxCode;
	}

	public String getReqTime() {
		return reqTime;
	}

	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	






}
