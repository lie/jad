package com.jad.commons.web.api.util;

import javax.servlet.http.HttpServletRequest;

import com.jad.commons.web.api.req.MsgReqHead;

public class HttpRequestUtil {
	
	
	public static final String JSON_REQUEST_BODY = "JSON_REQUEST_BODY";
	public static final String JSON_REQUEST_HEAD = "JSON_REQUEST_HEAD";
	public static final String DATA_NEED_UNPACK = "DATA_NEED_UNPACK";
	
	
	public static MsgReqHead getReqHead(HttpServletRequest httpReq) {
		return (MsgReqHead) httpReq.getAttribute(JSON_REQUEST_HEAD);
	}

}
