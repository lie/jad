package com.jad.commons.web.api.req;

public class PageableReqBody  extends MsgReqBody{

	private Integer pageNo;//	Integer
	private Integer pageSize;//	Integer
	
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	
}
