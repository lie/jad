package com.jad.commons.validator.annotation;

/**
 * 只有实现该接口的类才可以用此annotation方式校验
 */
public interface MsgValidable {

	
	void validate();
	
}
