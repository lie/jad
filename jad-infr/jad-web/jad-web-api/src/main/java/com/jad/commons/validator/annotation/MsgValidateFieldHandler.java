package com.jad.commons.validator.annotation;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MsgValidateFieldHandler implements MsgValidateHandler {

	private final static Logger logger = LoggerFactory.getLogger(MsgValidateFieldHandler.class);

	/*
	 */
	private void checkCanNull(Object dest, String fieldDesc,MsgValidateField a) {
		if (!a.canNull() && dest == null ) {
			String error = "[" + fieldDesc + "]不能为空";
			logger.error(error);
			throw new MsgAnnValidateException(error);
		}
	}

	private void checkCanBlank(Object dest, String fieldDesc,MsgValidateField a) {
		if (!a.canBlank() && (dest == null || "".equals(dest.toString().trim()))) {
			String error = "[" + fieldDesc + "]不能为空";
			logger.error(error);
			throw new MsgAnnValidateException(error);
		}
		
	}

	@SuppressWarnings("rawtypes")
	private void checkMinAndMaxValue(Object dest, String fieldDesc,MsgValidateField a,Class fieldType) {
		if(dest==null || ( isBlankStr(a.minValue()) && isBlankStr(a.maxValue())) ){
			return;
		}
		if (Integer.class.isAssignableFrom(fieldType)) {
			int val=(Integer)dest;
			if(!isBlankStr(a.minValue())){
				int minVal=Integer.parseInt(a.minValue());
				if(val<minVal){
					String error = "[" + fieldDesc + "]值范围超限，不能低于["+minVal+"]";
					logger.error(error);
					throw new MsgAnnValidateException(error);
				}
			}
			if(!isBlankStr(a.maxValue())){
				int maxVal=Integer.parseInt(a.maxValue());
				if(val>=maxVal){
					String error = "[" + fieldDesc + "]值范围超限，不能超过["+maxVal+"]";
					logger.error(error);
					throw new MsgAnnValidateException(error);
				}
			}
			
		}else if(Long.class.isAssignableFrom(fieldType)){
			long val=(Long)dest;
			
			if(!isBlankStr(a.minValue())){
				long minVal=Long.parseLong(a.minValue());
				if(val<minVal){
					String error = "[" + fieldDesc + "]值范围超限，不能低于["+minVal+"]";
					logger.error(error);
					throw new MsgAnnValidateException(error);
				}
			}
			if(!isBlankStr(a.maxValue())){
				long maxVal=Long.parseLong(a.maxValue());
				if(val>=maxVal){
					String error = "[" + fieldDesc + "]值范围超限，不能超过["+maxVal+"]";
					logger.error(error);
					throw new MsgAnnValidateException(error);
				}
			}
			
		}else if(Double.class.isAssignableFrom(fieldType)){
			double val=(Double)dest;
			if(!isBlankStr(a.minValue())){
				double minVal=Double.parseDouble(a.minValue());
				if(val<minVal){
					String error = "[" + fieldDesc + "]值范围超限，不能低于["+minVal+"]";
					logger.error(error);
					throw new MsgAnnValidateException(error);
				}
			}
			if(!isBlankStr(a.maxValue())){
				double maxVal=Double.parseDouble(a.maxValue());
				if(val>=maxVal){
					String error = "[" + fieldDesc + "]值范围超限，不能超过["+maxVal+"]";
					logger.error(error);
					throw new MsgAnnValidateException(error);
				}
			}
			
			
		}else{
			logger.warn("不校验字段类型为["+fieldType.getName()+"]的最小值和最大值");
		}
		
		

	}
	
	private boolean isBlankStr(Object v){
		return v==null || "".equals(v.toString().trim());
	}
	
	
	private void checkMinAndMaxLen(Object dest, String fieldDesc,MsgValidateField a,Class fieldType) {
		if(dest==null || ( isBlankStr(a.minLen()) && isBlankStr(a.maxLen())) ){
			return;
		}
		if (String.class.isAssignableFrom(fieldType)) {
			String val=(String)dest;
			int len=val.length();
			
			if(a.minLen()>-1){
				int minLen=a.minLen();
				if(len<minLen){
					String error = "[" + fieldDesc + "]代值范围超限，不能低于["+minLen+"]";
					logger.error(error);
					throw new MsgAnnValidateException(error);
				}
			}
			if(a.maxLen()>-1){
				int maxLen=a.maxLen();
				if(len>=maxLen){
					String error = "[" + fieldDesc + "]值范围超限，不能超过["+maxLen+"]";
					logger.error(error);
					throw new MsgAnnValidateException(error);
				}
			}
			
		}else{
			logger.warn("不校验字段类型为["+fieldType.getName()+"]的最小长度和最大长度");
		}
	}
	

	private void checkIsMustNumberic(Object dest, String fieldDesc,MsgValidateField a,Class fieldType) {
		if(dest==null  ){
			return;
		}
		if (Integer.class.isAssignableFrom(fieldType)) {
			return;
		}else if(Long.class.isAssignableFrom(fieldType)){
			return ;
		}else if(Double.class.isAssignableFrom(fieldType)){
			return ;
		}else if(String.class.isAssignableFrom(fieldType)){
			if(a.isMustNumberic() && !isNumeric(dest.toString())){
				String error = "[" + fieldDesc + "]字段的值必须为数字";
				logger.error(error);
				throw new MsgAnnValidateException(error);
			}
			return ;
		}
		else{
			logger.warn("不校验字段类型为["+fieldType.getName()+"]是否为数字");
		}
		
		
	}

	private void checkIsMustInteger(Object dest, String fieldDesc,MsgValidateField a,Class fieldType) {
		if(dest==null  ){
			return;
		}
		if (Integer.class.isAssignableFrom(fieldType)) {
			return;
		}else if(Long.class.isAssignableFrom(fieldType)){
			return ;
		}else if(Double.class.isAssignableFrom(fieldType)){
			return ;
		}else if(String.class.isAssignableFrom(fieldType)){
			if(a.isMustInteger() && !isInteger(dest.toString())){
				String error = "[" + fieldDesc + "]字段的值必须为整数";
				logger.error(error);
				throw new MsgAnnValidateException(error);
			}
			return ;
		}
		else{
			logger.warn("不校验字段类型为["+fieldType.getName()+"]是否为数字");
		}
		

	}

	private void check(MsgValidable filter, Field field) {

		MsgValidateField a = field.getAnnotation(MsgValidateField.class);

		String fieldDesc = a.fieldDesc();
		if (fieldDesc == null || "".equals(fieldDesc.trim())) {
			fieldDesc = field.getName();
		}

		Object dest = null;
		try {
			dest = GetFiledValue.getField(filter, field.getName());
		} catch (Exception ex) {
			logger.error("无法读取[" + field.getName() + "]属性值，请确定包含getter方法");
			throw new MsgAnnValidateException(ex.getMessage(), ex);
		}

		checkCanNull(dest, fieldDesc,a);
		checkCanBlank(dest, fieldDesc,a);
		checkMinAndMaxValue(dest, fieldDesc,a,field.getType());
		checkMinAndMaxLen(dest, fieldDesc,a,field.getType());
		
		checkIsMustNumberic(dest, fieldDesc,a,field.getType());
		checkIsMustInteger(dest, fieldDesc,a,field.getType());

	}

	
	private static boolean isInteger(String value) {
		try {
			Integer.valueOf(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	private static boolean isNumeric(String value) {
		try {
			Double.valueOf(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	@Override
	public void validate(MsgValidable validatedObj, Field field) {
		if (field.isAnnotationPresent(MsgValidateField.class)) {
			check(validatedObj, field);
		}

	}

}
