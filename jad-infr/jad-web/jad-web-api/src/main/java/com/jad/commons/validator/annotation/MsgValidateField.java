package com.jad.commons.validator.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 统一的字段校验注解，用于取代原来部分校验类   20170919
 * 
 * @author Administrator
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface MsgValidateField {
	
	/**
	 * 字段描述
	 */
	String fieldDesc() default "";
	/**
	 * 能否为空
	 */
	boolean canBlank() default true;
	/**
	 * 能否为空
	 */
	boolean canNull() default true;
	
	/**
	 * 最小长度
	 * @return
	 */
	int minLen() default -1; //-1表示不限制
	/**
	 * 最大找度
	 * @return
	 */
	int maxLen() default -1;//-1表示不限制
	
	
	/**
	 * 最小值
	 * @return
	 */
	String minValue() default ""; 
	/**
	 * 最大值
	 * @return
	 */
	String maxValue() default "";
	/**
	 * 是否必须为数字
	 * @return
	 */
	boolean isMustNumberic() default false;
	/**
	 * 是否必须为整数
	 * @return
	 */
	boolean isMustInteger() default false;
}
