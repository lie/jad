package com.jad.commons.web.api.req;

import java.io.Serializable;

import com.jad.commons.validator.annotation.MsgAnnotationValidator;
import com.jad.commons.validator.annotation.MsgValidable;

public class MsgReqBody implements Serializable,MsgValidable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MsgReqBody(){
	}
	
	public void validate(){
		MsgAnnotationValidator.getInstance().validate(this);
	}
	


}
