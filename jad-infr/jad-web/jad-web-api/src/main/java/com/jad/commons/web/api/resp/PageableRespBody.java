package com.jad.commons.web.api.resp;


public class PageableRespBody extends MsgRespBody {
	

	private Integer pageNo;//	Integer
	private Integer pageSize;//	Integer
	private Long count;//	Integer
	
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}

}
