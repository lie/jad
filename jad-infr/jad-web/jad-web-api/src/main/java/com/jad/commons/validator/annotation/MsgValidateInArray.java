package com.jad.commons.validator.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <li>检查属性值必须在某个字符串数组中</li>
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface MsgValidateInArray {

	String message() default "";

	String[] value();
	/**
	 * 能否为空
	 */
	boolean canBlank() default true;

}
