package com.jad.commons.validator.annotation;

import java.lang.reflect.Field;

/**
 * 
 */
public interface MsgValidateHandler {
	void validate(MsgValidable validatedObj, Field field);
}
