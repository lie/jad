package com.jad.commons.validator.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface MsgValidateIsEmail {
	String message() default "邮箱格式不正确";
	/**
	 * 能否为空
	 */
	boolean canBlank() default true;
}
