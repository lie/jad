package com.jad.commons.validator.annotation;

import java.lang.reflect.Method;

/**
 * <li>获得待校验的属性</li>
 * 
 */
public class GetFiledValue {

	/**
	 * 获得待校验的属性值
	 * 
	 * @param filter
	 * @param field
	 * @return
	 * @throws Exception
	 */
	public static Object getField(MsgValidable filter, String field) throws Exception {
		String firstLetter = field.substring(0, 1).toUpperCase();
		String getMethodName = "get" + firstLetter + field.substring(1);
		Method getMethod = filter.getClass().getMethod(getMethodName);
		Object returnValue = getMethod.invoke(filter);
		return returnValue;
	}
}
