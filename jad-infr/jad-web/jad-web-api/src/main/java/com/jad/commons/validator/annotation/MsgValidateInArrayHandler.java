package com.jad.commons.validator.annotation;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MsgValidateInArrayHandler implements MsgValidateHandler {

	private final static Logger logger = LoggerFactory.getLogger(MsgValidateInArrayHandler.class);
    
	private void checkIsInArray(MsgValidable filter, Field field) {
		
		MsgValidateInArray a=field.getAnnotation(MsgValidateInArray.class);
		String message = a.message();
		String[] valArr = a.value();
		if (valArr == null || valArr.length == 0) {
			message = "校检[" + field.getName() + "] 是否在给定数组中是出现导常";
			logger.error(message);
			throw new MsgAnnValidateException(message);
		}
		Object dest = null;
		try {
			dest = GetFiledValue.getField(filter, field.getName());
		} catch (Exception ex) {
			// logger.severe("校检["+field.getName()+"] 是否在给定数组中是出现导常");
			throw new MsgAnnValidateException(message + ",原因:" + ex.getMessage(), ex);
		}
		if (dest == null || "".equals(dest)){
			if(!a.canBlank()){
				message = "[" + field.getName() + "] 不能为空";
				logger.error(message);
				throw new MsgAnnValidateException(message);
			}
		}else if (dest instanceof String) {
			String itemVal = dest.toString().trim();
			if ((itemVal==null || "".equals(itemVal.trim())) && !a.canBlank() ){
				message = "[" + field.getName() + "] 不能为空";
				logger.error(message);
				throw new MsgAnnValidateException(message);
			}
			boolean flag = false;
			for (String str : valArr) {
				if (itemVal.equals(str)) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				StringBuffer arrStr = new StringBuffer("{");
				for (String str : valArr)
					arrStr.append(str + ",");
				if (arrStr.charAt(arrStr.length() - 1) == ',')
					arrStr.deleteCharAt(arrStr.length() - 1);
				arrStr.append("}");
				throw new MsgAnnValidateException(message + ",[" + itemVal + "]不在给定的数组：" + arrStr.toString() + "中");
			}
		} else {
			logger.error("校检[" + field.getName() + "] 的格式时出现异常");
			throw new MsgAnnValidateException("该注解只能用来校验类型为 String 的属性");
		}

	}

	public void validate(MsgValidable validatedObj, Field field) {
		// logger.info("校验["+field.getName()+"] 是否在给定数组中");
		if (field.isAnnotationPresent(MsgValidateInArray.class)) {
			checkIsInArray(validatedObj, field);
		}

	}

}
