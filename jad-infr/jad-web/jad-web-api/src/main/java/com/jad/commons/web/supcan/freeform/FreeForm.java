/**
 */
package com.jad.commons.web.supcan.freeform;

import com.jad.commons.web.supcan.common.Common;
import com.jad.commons.web.supcan.common.properties.Properties;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 */
@XStreamAlias("FreeForm")
public class FreeForm extends Common {

	public FreeForm() {
		super();
	}
	
	public FreeForm(Properties properties) {
		this();
		this.properties = properties;
	}
	
}
