package com.jad.commons.web.api;

import java.util.LinkedHashMap;
import java.util.Map;

public class MsgRespCode {
	
	private String code;
	private String msg;
	
	private static final Map<String,MsgRespCode>MAP=new LinkedHashMap<String,MsgRespCode>();
	
	public static MsgRespCode SUCCESS=new MsgRespCode("200","操作成功");
	
	public static MsgRespCode PARAM_FAIL=new MsgRespCode("400","参数错误");
	
	public static MsgRespCode UN_AUTHORIZED=new MsgRespCode("403","没有权限");
	
	public static MsgRespCode NO_LOGIN=new MsgRespCode("401","没有登录或会话超时");
	
	public static MsgRespCode BUSI_ERROR=new MsgRespCode("406","业务异常");
	
	public static MsgRespCode NET_ERROR=new MsgRespCode("502","网络异常");
	
	public static MsgRespCode NET_TIMEOUT=new MsgRespCode("503","网络连接超时");
	
	public static MsgRespCode FAIL_NOKNOW=new MsgRespCode("500","未知异常");
	
	private MsgRespCode(String code,String msg){
		this.code=code;this.msg=msg;
		MAP.put(code, this);
	}

	
	public static MsgRespCode valueOf(String code){
		return MAP.get(code);
	}
	
	

	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}

}
