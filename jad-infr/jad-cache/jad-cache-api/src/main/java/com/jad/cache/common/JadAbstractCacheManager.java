package com.jad.cache.common;

import org.apache.log4j.Logger;
import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;
import org.springframework.util.Assert;

/**
 * 借用 spring中的 AbstractCacheManager 对JadCacheManager进行抽象实现 
 * @author hechuan
 *
 */
public abstract class JadAbstractCacheManager extends AbstractCacheManager implements JadCacheManager{

	
	private static Logger logger = Logger.getLogger(JadAbstractCacheManager.class);
	
	/**
	 * 所属客户端
	 */
	private CacheClient cacheClient;
	
	/**
	 * 获得缓存
	 * @param name
	 * @return
	 */
	public Cache initCache(JadCache jc){
		String name = jc.getName();
		Assert.notNull(name, "name不能为空");
		if(exists(name)){
			logger.warn("缓存已存在，不重复初始化,"+this.getLoggerInfo(name));
			return this.getCache(name);
		}
		logger.debug("初始化缓存,"+this.getLoggerInfo(name));
		Cache cache = newCache(jc);//new一个新的
		super.addCache(cache); //添加到缓存管理器
		return cache;
		
	}
	
	
	
	private boolean exists(String name){
		return this.getCacheNames().contains(name);
	}
	
	protected String getLoggerInfo(String cacheName){
		StringBuffer sb = new StringBuffer();
		if(this.cacheClient != null){
			sb.append("clientName:").append(this.cacheClient.getClientName());
		}
		if(cacheName!=null){
			sb.append(",cacheName:").append(cacheName);
		}
		return sb.toString();
	}

	public CacheClient getCacheClient() {
		return cacheClient;
	}

	public void setCacheClient(CacheClient cacheClient) {
		this.cacheClient = cacheClient;
	}


}
