package com.jad.cache.common;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.util.Assert;

import com.jad.cache.common.api.CacheClientHelper;
import com.jad.cache.common.api.CacheHelper;

/**
 * 这是整个框架的主体缓存管理器，可同时管理ehcache,memcache及map自行实现的缓存
 * 
 * 这个是一个单例，在CacheClientManager实例初始化时被自动生成并注入到srping上下文中
 * 
 * 而且整个应用系统中所有CacheClient管理的CacheManager实例最终都要注册 到这个管理器的cacheManagerMap集合中
 * 以实现同时使用多CacheManager 
 * 
 * @author Administrator
 *
 */
public class MasterCacheManager implements CacheManager,BeanDefinitionRegistryPostProcessor{

	/**
	 * 所有CacheClient管理的CacheManager集合
	 */
	private final Map<String,JadCacheManager> cacheManagerMap = new HashMap<String,JadCacheManager>();
	
	/**
	 * 所有注册CacheClient集合
	 */
	private final Map<String,CacheClient> cacheClientMap = new HashMap<String,CacheClient>();
	
	private static final Logger logger = LoggerFactory.getLogger(MasterCacheManager.class);
	
	/**
	 * 默认管理器
	 * 在单客户端的情况下，这个引用对应，系统唯一的CacheClient实例所控制的CacheManager
	 * 如果多客端，需要通过配置自行指定一个默认的CacheManager
	 * (在CacheClientManager可通过defCacheClientName指定默认的CacheClient，这个引用就指向默认CacheClient所控制的CacheManager)
	 */
	private JadCacheManager defCacheManager;
	
	private String defClientName;

	@SuppressWarnings({ "rawtypes", "unused" })
	@Override
	public Cache getCache(String name) {
		Map map = this.cacheManagerMap;
		for(Map.Entry<String, JadCacheManager>ent:this.cacheManagerMap.entrySet()){
			CacheManager cm =ent.getValue();
			Collection<String>names=cm.getCacheNames();
			if(ent.getValue().getCacheNames().contains(name)){
				return ent.getValue().getCache(name);
			}
		}
		
		logger.debug("没有找到名称为:"+name+"的缓存，准备从默认客户端:"+defClientName+"中生成");
		
		Cache cache = getDefCacheManager().getCache(name);
		
		if(cache == null ){
			throw new CacheException("找不到名称为:"+name+"的缓存,请检查是否配置了此缓存，"
					+ "或确保默认客户端:"+defClientName+"具备自动创建缓存的能力");
		}
		
		return cache;
		
	}
	
	
	/**
	 * 注册客户端所管理的CacheManager
	 * CacheClient初始化CacheManager后需要调用此方法注册到此实例中
	 * @param clientName
	 * @param manager
	 */
	public void registry(CacheClient client,JadCacheManager manager){
		Assert.notNull(client);
		Assert.notNull(manager);
		Assert.notNull(client.getClientName());
		
		if(cacheManagerMap.containsKey(client.getClientName())){
			throw new CacheException("已存在缓存客户端["+client.getClientName()+"],不能重复注册");
		}
		if(manager instanceof MasterCacheManager){
			throw new CacheException("不支持注册GroupCacheManager类型的客户端，请检查配置");
		}
		cacheClientMap.put(client.getClientName(), client);
		cacheManagerMap.put(client.getClientName(), manager);
	}

	@Override
	public Collection<String> getCacheNames() {
		Set<String> names = new LinkedHashSet<String>();
		for (CacheManager manager : this.cacheManagerMap.values()) {
			names.addAll(manager.getCacheNames());
		}
		return Collections.unmodifiableSet(names);
	}
	
	/**
	 * 获得默认的JadCacheManager
	 * @return
	 */
	public JadCacheManager getDefCacheManager() {
		if(defCacheManager==null){
			defCacheManager = cacheManagerMap.get(defClientName);
			if(defCacheManager == null){
				throw new CacheException("没有跟据默认客户端名称:"+defClientName+"找到对应的缓存管理器");
			}
		}
		
		return defCacheManager;
	}

	public void setDefCacheManager(JadCacheManager defCacheManager) {
		this.defCacheManager = defCacheManager;
	}

	public String getDefClientName() {
		return defClientName;
	}

	public void setDefClientName(String defClientName) {
		this.defClientName = defClientName;
	}
	
	/**
	 * 获得所客户端名字
	 * @return
	 */
	public Collection<String>getClientNames(){
		return Collections.unmodifiableSet(cacheManagerMap.keySet());
	}
	
	public CacheClient getClient(String name){
		CacheClient client = cacheClientMap.get(name);
		if(client == null ){
			throw new CacheException("找不到名称为:"+name+"客户端，可能没有注册");
		}
		return client;
	}

	
	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		CacheClientHelper.init(this);
		CacheHelper.init(this);
	}
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
	}


}
