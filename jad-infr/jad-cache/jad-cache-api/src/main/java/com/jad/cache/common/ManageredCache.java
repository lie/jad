package com.jad.cache.common;

import java.util.Set;

import org.springframework.cache.Cache;


/**
 * 受客户端管理的Cache
 * 
 * 所有ehcache.xml中每个cache或者memcache.xml中的每个client
 * 都有一个此类型的实例，而且每个实例都被一个CacheCleint管理
 *
 */
public interface ManageredCache extends Cache {
	
	/**
	 * 被管理的Client
	 * @return
	 */
	public CacheClient getCacheClient();
	
	/**
	 * 能否缓存null
	 * @return
	 */
	public boolean isAllowNullValues();
	
	/**
	 * 存活时间
	 * @return
	 */
	public int getActivityTime();
	
	
	/**
	 * 获得缓存中的值，用于取代父类中的get方法
	 * @param key
	 * @return
	 */
	public CacheValue getVal(String key);
	
	public void put(Object key, Object value);
	
	public void put(Object key, Object value,int activityTime);

	/**
	 * 清空客户端所有缓存数据
	 */
	public void clear();
	
	/**
	 * 查看客户端缓存中条目的数量
	 * @param name 缓存名称
	 * 此操作在memcache中可能会引发对缓存容器中的对像遍历，慎用
	 */
	public Integer size();
	
	
	/**
	 * 返回所有key列表
	 * 此操作在memcache中可能会引发对缓存容器中的对像遍历，慎用
	 * @return
	 */
	public Set<String> keySet();
	
}
