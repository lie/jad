package com.jad.cache.common;


/**
 * 缓存客户端
 * 
 * jad-cache 缓存框架中以client作为与缓存容器交互的入口
 * 
 * 每个client实例唯一管理一个CacheMenager，控制它的启停
 * 每个client实例可以操作若干个Cache实例(可在配置此client的bean中直接配置，也可以通过ehcache.xml或memcache.xml中配置)
 * client本身不直接操作缓存容器，所有对Cache的直接操作转交给CacheMenager
 * 
 * 一个应用系统中，可以有多个相同或者不同的CacheClient实例,这使得系统可以同时使用 不同厂商的缓存
 * 应用中所有CacheClient实例被  CacheClientManager统一管理
 * 
 */
public interface CacheClient {

	/**
	 * 获得所控制的缓存管理器
	 * @return
	 */
	public JadCacheManager getCacheManager();
	
	
	/**
	 * 返回存活时间
	 * @return
	 */
	public Integer getDefActivityTime();
	
	/**
	 * 设默认存活时间
	 * @param defActivityTime
	 */
	public void setDefActivityTime(Integer defActivityTime);
	
	/**
	 * 能否缓存空值
	 * 当client缺省配置allowNullValues参数时，此方法返回null
	 * @return
	 */
	public Boolean getAllowNullValues();
	
	/**
	 * 值为true时，此客户端所管理的缓存可保存null值
	 * @param allowNullValues
	 */
	public void setAllowNullValues(Boolean allowNullValues);
	
	/**
	 * 能否缓存空值
	 * 当client缺省配置allowNullValues参数时，此方法返回false
	 * @return
	 */
	public boolean isAllowNullValues();
	
	
	/**
	 * 能否自动创建缓存
	 * 当client缺省配置autoCreateCache参数时，此方法返回null
	 * @return
	 */
	public Boolean getAutoCreateCache();
	
	/**
	 * 如果设为true,在此client找不到某个name的缓存时，会自动创建
	 * @param autoCreateCache
	 */
	public void setAutoCreateCache(Boolean autoCreateCache);
	
	/**
	 * 能否自动创建缓存
	 * 当client缺省配置autoCreateCache参数时，此方法返回false
	 * @return
	 */
	public boolean isAutoCreateCache();
	
	/**
	 * 获得此客户端名称
	 * @return
	 */
	public String getClientName();
	
	/**
	 * 设置此客户端名称
	 * @return
	 */
	public void setClientName(String clientName);
	
	/**
	 * 缓存客户端启动
	 */
	public void start();
	
	/**
	 * 缓存客户端停止
	 */
	public void stop();
	
	/**
	 * 是否启动
	 * @return
	 */
	public boolean isStarted() ;
	
	/**
	 * 是否自动启动
	 * @return
	 */
	public boolean isAutoStart();

	
	
}
