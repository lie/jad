package com.jad.commons.security.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import com.jad.commons.context.Constants;
import com.jad.core.sys.vo.UserVo;

public class SecurityHelper {
	

	/**
	 * 获取SESSIONID
	 */
	public static String getSessionId() {
		return (String) getSession().getId();
	}
	
	/**
	 * 返回当前Session
	 * @return
	 */
	public static Session getSession(){
		return getSubject().getSession();
	}
	
	/**
	 * 返回当前Session
	 * @param create 如果没有，是否创建一个
	 * @return
	 */
	public static Session getSession(boolean create){
		return getSubject().getSession(create);
	}
	
	
	/**
	 * 获得当前登录的userid
	 * @return
	 */
	public static String getCurrentUserId(){
		Principal p = getPrincipal();
		if(p==null){
			throw new AuthenticationException("还没有登录");
		}
		return p.getId();
	}
	
	/**
	 * 获得当前登录的用户
	 * @return
	 */
	public static UserVo getCurrentUser(){
		Principal p = getPrincipal();
		if(p==null){
			throw new AuthenticationException("还没有登录");
		}
		return (UserVo)p.getData();
	}
	
	
	/**
	 * 获取授权主要对象
	 */
	public static Subject getSubject(){
		return SecurityUtils.getSubject();
	}
	
	/**
	 * 获取当前登录者对象
	 */
	public static Principal getPrincipal(){
		return (Principal)getSubject().getPrincipal();
	}
	
	/**
	 * 返回验证码
	 * @return
	 */
	public static String getSessionValidateCode(){
		Session session = getSession();
		if(session!=null){
			return (String)session.getAttribute(Constants.VALIDATE_CODE);
		}
		return null;
	}
	
	/**
	 * 权限检查
	 * @param permission
	 * @return
	 */
	public static boolean isPermitted(String permission){
		return getSubject().isPermitted(permission);
	}
	
	/**
	 * 是否登录
	 * @return
	 */
	public static boolean isAuthenticated(){
		return getSubject().isAuthenticated();
	}
	
	/**
	 * 退出登录
	 */
	public static void logout(){
		getSubject().logout();
	}
	
}


