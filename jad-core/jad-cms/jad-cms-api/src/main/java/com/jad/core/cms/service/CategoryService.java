/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service;

import java.util.List;

import com.jad.commons.service.TreeService;
import com.jad.core.cms.vo.CategoryVo;

/**
 * 栏目Service
 * @author ThinkGem
 * @version 2013-5-31
 */
public interface CategoryService extends TreeService<CategoryVo,String> {
	
//	public List<CategoryVo> findByUser(boolean isCurrentSite, String module);

//	public List<CategoryVo> findByParentId(String parentId, String siteId);;
	
//	public Page<CategoryVo> find(Page<CategoryVo> page, CategoryQo category) ;
	
//	/**
//	 * 通过编号获取栏目列表
//	 */
//	public List<CategoryVo> findByIds(String ids) ;
	
}
