/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.dao;

import com.jad.core.cms.entity.SiteEo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 站点DAO接口
 * @author ThinkGem
 * @version 2013-8-23
 */
@JadDao
public interface SiteDao extends JadEntityDao<SiteEo,String> {

}
