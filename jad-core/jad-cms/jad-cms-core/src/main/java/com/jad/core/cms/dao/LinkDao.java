/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.dao;

import java.util.List;

import com.jad.core.cms.entity.LinkEo;
import com.jad.core.cms.vo.LinkVo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 链接DAO接口
 * @author ThinkGem
 * @version 2013-8-23
 */
@JadDao
public interface LinkDao extends JadEntityDao<LinkEo,String> {
	
//	public List<LinkEo> findByIdIn(String[] ids);
//	{
//		return find("front Like where id in (:p1)", new Parameter(new Object[]{ids}));
//	}
	
	public int updateExpiredWeight();
//	{
//		return update("update Link set weight=0 where weight > 0 and weightDate < current_timestamp()");
//	}
//	public List<Link> fjindListByEntity();
}
