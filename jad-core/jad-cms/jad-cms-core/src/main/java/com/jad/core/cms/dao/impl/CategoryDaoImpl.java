package com.jad.core.cms.dao.impl;

import com.jad.commons.dao.AbstractTreeDao;
import com.jad.core.cms.dao.CategoryDao;
import com.jad.core.cms.entity.CategoryEo;
import com.jad.dao.annotation.JadDao;

@JadDao("categoryDao")
public class CategoryDaoImpl extends AbstractTreeDao<CategoryEo,String>implements CategoryDao{


//	@Override
//	public List<CategoryEo> findModule(CategoryQo category) {
//		
//		Assert.notNull(category);
//		
//		List<Object>params=Lists.newArrayList();
//		
//		StringBuffer sb=new StringBuffer();
//		sb.append(SqlHelper.getSelectSql(CategoryEo.class, "a", params));
//		
//		sb.append(" where a.del_flag = ? ");
//		params.add(Constants.DEL_FLAG_NORMAL);
//		
//		if(StringUtils.isNotBlank(category.getSiteId())){
//			sb.append(" AND a.site_id  = ?");
//			params.add(category.getSiteId());
//		}
//		if(StringUtils.isNotBlank(category.getParentId())){
//			sb.append(" AND a.parent_id  = ?");
//			params.add(category.getParentId());
//		}
//		if(StringUtils.isNotBlank(category.getInMenu())){
//			sb.append(" AND a.in_menu  = ?");
//			params.add(category.getInMenu());
//		}
//		sb.append(" ORDER BY a.site_id,a.sort ASC");
//		
//		return super.findBySql(sb.toString(), params);
//	}

//	@Override
//	public List<CategoryEo> findByModule(String module) {
//		
//		List<Object>params=Lists.newArrayList();
//		
//		StringBuffer sb=new StringBuffer();
//		sb.append(SqlHelper.getSelectSql(CategoryEo.class, "a", params));
//		
//		sb.append(" where a.del_flag = ? ");
//		params.add(Constants.DEL_FLAG_NORMAL);
//		
//		sb.append(" and (a.module='' or a.module=? ) ");
//		params.add( module );
//		
//		sb.append(" ORDER BY a.site_id,a.sort ASC");
//		
//		return super.findBySql(sb.toString(), params);
//	}

//	@Override
//	public List<CategoryEo> findByParentId(String parentId, String isMenu) {
//		
//		List<Object>params=Lists.newArrayList();
//		
//		StringBuffer sb=new StringBuffer();
//		sb.append(SqlHelper.getSelectSql(CategoryEo.class, "a", params));
//		
//		sb.append(" where a.del_flag = ? and a.parent_id=? ");
//		params.add(Constants.DEL_FLAG_NORMAL);
//		params.add(parentId);
//		
//		if(StringUtils.isNotBlank(isMenu)){
//			sb.append(" and a.inMenu=?  ");
//			params.add( isMenu );
//		}
//		
//		sb.append(" ORDER BY a.site_id,a.sort ASC");
//		
//		return super.findBySql(sb.toString(), params);
//	}

//	@Override
//	public List<CategoryEo> findByParentIdAndSiteId(String parentId,String siteId) {
//		
//		List<Object>params=Lists.newArrayList();
//		
//		StringBuffer sb=new StringBuffer();
//		sb.append(SqlHelper.getSelectSql(CategoryEo.class, "a", params));
//		
//		sb.append(" where a.del_flag = ? and a.parent_id=? ");
//		params.add(Constants.DEL_FLAG_NORMAL);
//		params.add(parentId);
//		
//		if(StringUtils.isNotBlank(siteId)){
//			sb.append(" and a.site_id=?  ");
//			params.add( siteId );
//		}
//		
//		sb.append(" ORDER BY a.site_id,a.sort ASC");
//		
//		return super.findBySql(sb.toString(), params);
//		
//	}

//	@Override
//	public List<Map<String, Object>> findStats(String sql) {
//		// TODO Auto-generated method stub
//		return null;
//	}


}
