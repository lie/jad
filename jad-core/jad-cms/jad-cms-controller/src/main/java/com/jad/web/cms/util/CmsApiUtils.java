package com.jad.web.cms.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;

import com.jad.commons.json.JsonMapper;
import com.jad.commons.utils.StringUtils;
import com.jad.core.cms.vo.CategoryVo;

public class CmsApiUtils {

	 public static void addViewConfigAttribute(Model model, String param){
	        if(StringUtils.isNotBlank(param)){
	            @SuppressWarnings("rawtypes")
				Map map = JsonMapper.getInstance().fromJson(param, Map.class);
	            if(map != null){
	                for(Object o : map.keySet()){
	                    model.addAttribute("viewConfig_"+o.toString(), map.get(o));
	                }
	            }
	        }
	    }

	    public static void addViewConfigAttribute(Model model, CategoryVo category){
	        List<CategoryVo> categoryList = new ArrayList();
	        CategoryVo c = category;
	        boolean goon = true;
	        do{
	            if(c.getParent() == null || c.getParent().isRoot()){
	                goon = false;
	            }
	            categoryList.add(c);
	            c = c.getParent();
	        }while(goon);
	        Collections.reverse(categoryList);
	        for(CategoryVo ca : categoryList){
	        	addViewConfigAttribute(model, ca.getViewConfig());
	        }
	    }

}
