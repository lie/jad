/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Constants;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.web.api.Result;
import com.jad.core.cms.qo.GuestbookQo;
import com.jad.core.cms.qo.GuestbookQo;
import com.jad.core.cms.service.GuestbookService;
import com.jad.core.cms.vo.GuestbookVo;
import com.jad.core.cms.vo.GuestbookVo;
import com.jad.web.mvc.BaseController;

/**
 * 留言Controller
 * @author ThinkGem
 * @version 2013-3-23
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/guestbook")
@Api(description = "留言管理")
public class GuestbookController extends BaseController {

	@Autowired
	private GuestbookService guestbookService;
	
//	@Autowired
//	private SystemService systemService;
	
//	@ModelAttribute
//	public GuestbookVo get(@RequestParam(required=false) String id) {
//		if (StringUtils.isNotBlank(id)){
//			return guestbookService.findById(id);
//		}else{
//			return new GuestbookVo();
//		}
//	}
	
	
	/**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:guestbook:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看留言信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<GuestbookVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(guestbookService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @param userQo
	 * @param pageQo
	 * @return
	 */
	@RequiresPermissions("sys:guestbook:view")
	@ResponseBody
	@RequestMapping(value = {"findPage"})
	@ApiOperation(value = "查询留言列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<Page<GuestbookVo>> findPage(GuestbookQo guestbookQo,PageQo pageQo){
		Page<GuestbookVo> page = guestbookService.findPage(pageQo, guestbookQo);
		return Result.newSuccResult(page);
	}
	
	/**
	 * 修改
	 * @param guestbookVo
	 * @return
	 */
	@RequiresPermissions("sys:guestbook:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改留言信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(GuestbookVo guestbookVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, guestbookVo)){
			return result;
		}

		if(StringUtils.isBlank(guestbookVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		guestbookVo.setDelFlag(Constants.DEL_FLAG_NORMAL);
		preUpdate(guestbookVo,SecurityHelper.getCurrentUserId());
		guestbookService.update(guestbookVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:guestbook:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增留言信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(GuestbookVo guestbookVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (guestbookVo.getReUser() == null){
			guestbookVo.setReUser(SecurityHelper.getCurrentUser());
			guestbookVo.setReDate(new Date());
		}
		guestbookVo.setDelFlag(Constants.DEL_FLAG_NORMAL);
		
		if (!beanValidator(result, guestbookVo)){
			return result;
		}
		preInsert(guestbookVo,SecurityHelper.getCurrentUserId());
		guestbookService.add(guestbookVo);
		return result;
	}
	
	/**
	 * 删除或退回审核
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:guestbook:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除留言信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id,@RequestParam(required=false) Boolean isRe){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		GuestbookVo delVo = new GuestbookVo();
		delVo.setId(id);
		String delFlag = (isRe!=null && isRe.booleanValue())? Constants.DEL_FLAG_AUDIT:Constants.DEL_FLAG_DELETE;
		delVo.setDelFlag(delFlag);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		guestbookService.delete(delVo);
		return result;
	}
	
	
	@RequiresPermissions("cms:guestbook:view")
	@RequestMapping(value = {"list", ""})
	public String list(GuestbookQo guestbookQo, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<GuestbookVo> page = guestbookService.findPage(getPage(request, response), guestbookQo); 
        model.addAttribute("page", page);
		return "modules/cms/guestbookList";
	}

	@RequiresPermissions("cms:guestbook:view")
	@RequestMapping(value = "form")
	public String form(GuestbookQo guestbookQo, Model model) {
		GuestbookVo guestbookVo = new GuestbookVo();
		if (StringUtils.isNotBlank(guestbookQo.getId())){
			guestbookVo = guestbookService.findById(guestbookQo.getId());
		}
		return toForm(guestbookVo,model);
	}
	
	private String toForm(GuestbookVo guestbookVo, Model model){
		model.addAttribute("guestbookVo", guestbookVo);
		return "modules/cms/guestbookForm";
	}

	@RequiresPermissions("cms:guestbook:edit")
	@RequestMapping(value = "save")
	public String save(GuestbookVo guestbookVo, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, guestbookVo)){
//			return form(guestbookVo, model);
			return toForm(guestbookVo,model);
		}
		
		if(StringUtils.isNotBlank(guestbookVo.getId())){
			preUpdate(guestbookVo, SecurityHelper.getCurrentUserId());
			guestbookService.update(guestbookVo);
		}else{
			
			if (guestbookVo.getReUser() == null){
				guestbookVo.setReUser(SecurityHelper.getCurrentUser());
				guestbookVo.setReDate(new Date());
			}
			preInsert(guestbookVo, SecurityHelper.getCurrentUserId());
			guestbookService.add(guestbookVo);
		}
//		guestbookService.save(guestbook);
//		addMessage(redirectAttributes, DictUtils.getDictLabel(guestbook.getDelFlag(), "cms_del_flag", "保存")+"留言'" + guestbook.getName() + "'成功");
		addMessage(redirectAttributes, "操作成功");
		return "redirect:" + adminPath + "/cms/guestbook/?repage&status=2";
	}
	
	@RequiresPermissions("cms:guestbook:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id, 
			@RequestParam(required=false) Boolean isRe, RedirectAttributes redirectAttributes) {
//		guestbookService.delete(guestbookVo, isRe);
//		guestbookService.delete(guestbookVo);
		
		GuestbookVo delVo = new GuestbookVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		
		String delFlag = (isRe!=null && isRe.booleanValue())? Constants.DEL_FLAG_AUDIT:Constants.DEL_FLAG_DELETE;
		delVo.setDelFlag(delFlag);
		preUpdate(delVo, SecurityHelper.getCurrentUserId());
		guestbookService.update(delVo);
		
//		addMessage(redirectAttributes, (isRe!=null&&isRe?"恢复审核":"删除")+"留言成功");
		addMessage(redirectAttributes, "操作成功");
		
//		addMessage(redirectAttributes, (isRe!=null&&isRe?"恢复审核":"删除")+"留言成功");
		return "redirect:" + adminPath + "/cms/guestbook/?repage&status="+delFlag;
	}

}
