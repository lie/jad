package com.jad.bpm.dto;

import java.io.Serializable;

public class ActIdentityLink implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String type;

	/**
	 * If the identity link involves a user, then this will be a non-null id of
	 * a user. That userId can be used to query for user information through the
	 * {@link UserQuery} API.
	 */
	private String userId;

	/**
	 * If the identity link involves a group, then this will be a non-null id of
	 * a group. That groupId can be used to query for user information through
	 * the {@link GroupQuery} API.
	 */
	private String groupId;

	/**
	 * The id of the task associated with this identity link.
	 */
	private String taskId;

	/**
	 * The process definition id associated with this identity link.
	 */
	private String processDefinitionId;

	/**
	 * The process instance id associated with this identity link.
	 */
	private String processInstanceId;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

}
