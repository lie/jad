package com.jad.wx.mp.service;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.req.RecMsgsendEventReq;
import com.jad.wx.mp.req.UploadnewsReq;
import com.jad.wx.mp.req.UploadvideoReq;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.SendAllResp;
import com.jad.wx.mp.resp.UploadnewsResp;
import com.jad.wx.mp.resp.UploadvideoResp;


/**
 * 群发消息接口
 * @author hechuan
 *
 */
public interface MsgGroupSendService {
	
	/**
	 * 上传图文消息内的图片获取URL【订阅号与服务号认证后均可用】 
	  	http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN
		调用示例（使用curl命令，用FORM表单方式上传一个图片）：
		curl -F media=@test.jpg "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN"
	 * @param media 是 	调用接口凭证 
	 * @param access_token 是 	form-data中媒体文件标识，有filename、filelength、content-type等信息 
	 * @return String 正常返回url,否则返回失败响应码和响应信息
	 */
	String uploadimg(String media,String accessToken )throws WeixinMpException;
	
	/**
	 * 上传图文消息素材【订阅号与服务号认证后均可用】 
	 * http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=ACCESS_TOKEN
	 * @param req
	 * @param accessToken
	 * @return
	 * @throws WeixinMpException
	 */
	UploadnewsResp uploadnews(UploadnewsReq req,String accessToken )throws WeixinMpException;
	
	
	/**
	 * 上传视频
	 * 视频的media_id需通过POST请求到下述接口特别地得到： 
	 * https://file.api.weixin.qq.com/cgi-bin/media/uploadvideo?access_token=ACCESS_TOKEN POST
	 * @param req
	 * @param accessToken
	 * @return
	 */
	UploadvideoResp uploadvideo(UploadvideoReq req,String accessToken )throws WeixinMpException;
	
	/**
	 * 群发消息
	 * @param is_to_all 否 	用于设定是否向全部用户发送，值为true或false，选择true该消息群发给所有用户，
	 					选择false可根据group_id发送给指定群组的用户 
	 * @param group_id 否 	群发到的分组的group_id，参加用户管理中用户分组接口，若is_to_all值为true，可不填写group_id 
	 * @param msgtype  	是 	群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，
	 * 					图片为image，视频为video，卡券为wxcard 
	 * @param title 否 	消息的标题 
	 * @param description 否 	消息的描述 
	 * @param id
	 * @return
	 * @throws WeixinMpException
	 */
	SendAllResp sendAll(boolean is_to_all,String group_id,String msgtype,String title ,String description,String content,String access_token)throws WeixinMpException;
	
	/**
	 * 根据OpenID列表群发【订阅号不可用，服务号认证后可用】 
	 * http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=ACCESS_TOKEN
	 * @param touser  	填写图文消息的接收者，一串OpenID列表，OpenID最少2个，最多10000个 
	 * @param msgtype  	群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，视频为video，卡券为wxcard 
	 * @param title
	 * @param description
	 * @param id 
	 * @return
	 * @throws WeixinMpException
	 */
	SendAllResp send(String[] touser,String msgtype,String title ,String description,String content,String access_token)throws WeixinMpException;

	
	/**
	 * 删除群发【订阅号与服务号认证后均可用】 
		https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=ACCESS_TOKEN
		1、只有已经发送成功的消息才能删除
		2、删除消息是将消息的图文详情页失效，已经收到的用户，还是能在其本地看到消息卡片。
		3、删除群发消息只能删除图文消息和视频消息，其他类型的消息一经发送，无法删除。
		4、如果多次群发发送的是一个图文消息，那么删除其中一次群发，就会删除掉这个图文消息也，导致所有群发都失效
	 * @param msg_id 是 	发送出去的消息ID 
	 * @param access_token
	 * @throws WeixinMpException
	 * 正确时的JSON返回结果{"errcode":0,"errmsg":"ok"}
	 */
	CommonResp delete(String msg_id ,String access_token)throws WeixinMpException;
	
	/**
	 * 预览接口【订阅号与服务号认证后均可用】 
	 * http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=ACCESS_TOKEN
	 * @param touser
	 * @param msgtype
	 * @param content
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	SendAllResp send(String touser,String msgtype,String content,String access_token)throws WeixinMpException;
	
	/**
	 * 查询群发消息发送状态【订阅号与服务号认证后均可用】 
	 * http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/message/mass/get?access_token=ACCESS_TOKEN
	 * @param msg_id
	 * @param access_token
	 * @return msg_id 	群发消息后返回的消息id ,msg_status 消息发送后的状态，SEND_SUCCESS表示发送成功 
	 */
	String get(String msg_id ,String access_token) ;
	
	/**
	 * 处理事件推送群发结果 
	 * @param req
	 */
	void dealRecMsgsendEvent(RecMsgsendEventReq req);
	
}
