package com.jad.wx.mp.service;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.CreateGroupResp;
import com.jad.wx.mp.resp.CreateTagResp;
import com.jad.wx.mp.resp.GetBlackListResp;
import com.jad.wx.mp.resp.GetGroupResp;
import com.jad.wx.mp.resp.GetTagsResp;
import com.jad.wx.mp.resp.GetUserByTagidResp;
import com.jad.wx.mp.resp.GetUserGroupResp;
import com.jad.wx.mp.resp.GetUserInfoResp;
import com.jad.wx.mp.resp.GetUserListResp;
import com.jad.wx.mp.resp.GetidlistResp;

/**
 * 微信公众平台用户服务
 * @author hechuan
 *
 */
public interface WxUserService {
	
	/**
	 * 创建分组
	 * 一个公众账号，最多支持创建100个分组。 
	 * 
	 * http请求方式: POST（请使用https协议）
		https://api.weixin.qq.com/cgi-bin/groups/create?access_token=ACCESS_TOKEN
	 * @param name  	分组名字（30个字符以内） 
	 * @return
	 */
	CreateGroupResp createGroup(String name,String access_token)throws WeixinMpException;
	
	/**
	 * 查询所有分组
	 * http请求方式: GET（请使用https协议）
		https://api.weixin.qq.com/cgi-bin/groups/get?access_token=ACCESS_TOKEN
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	GetGroupResp getGroup(String access_token)throws WeixinMpException;
	
	/**
	 * 查询用户所在分组
	 * http请求方式: POST（请使用https协议）
		https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=ACCESS_TOKEN
		POST数据格式：json
		POST数据例子：{"openid":"od8XIjsmk6QdVTETa9jLtGWA6KBc"}
	 * @param openid
	 * @param access_token
	 * @return groupid
	 */ 
	GetUserGroupResp queryUserGroup(String openid,String access_token)throws WeixinMpException;
	
	/**
	 * 修改分组
	 * http请求方式: POST（请使用https协议）
		https://api.weixin.qq.com/cgi-bin/groups/update?access_token=ACCESS_TOKEN
		POST数据格式：json
		POST数据例子：{"group":{"id":108,"name":"test2_modify2"}}
	 * @param id
	 * @param name
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp modifyGroup(String id,String name,String access_token)throws WeixinMpException;
	
	/**
	 * 移动用户所在的分组
	 * http请求方式: POST（请使用https协议）
		https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=ACCESS_TOKEN
		POST数据格式：json
		POST数据例子：{"openid":"oDF3iYx0ro3_7jD4HFRDfrjdCM58","to_groupid":108}
	 * @param openid
	 * @param to_groupid
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp moveUserToGroup(String openid,String to_groupid ,String access_token)throws WeixinMpException;
	
	/**
	 * 批量移动用户分组
	 * http请求方式: POST（请使用https协议）
		https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=ACCESS_TOKEN
		POST数据格式：json
		POST数据例子：{"openid_list":["oDF3iYx0ro3_7jD4HFRDfrjdCM58","oDF3iY9FGSSRHom3B-0w5j4jlEyY"],"to_groupid":108}
	 * 
	 * @param openid_list 用户唯一标识符openid的列表（size不能超过50）
	 * @param to_groupid
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp moveUserToGroup(String[]openid_list ,String to_groupid ,String access_token)throws WeixinMpException;
	
	/**
	 * 删除分组
	 * http请求方式: POST（请使用https协议）
		https://api.weixin.qq.com/cgi-bin/groups/delete?access_token=ACCESS_TOKEN
		POST数据格式：json
		POST数据例子：{"group":{"id":108}}
	 * @param id
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp delGroup(String id,String access_token)throws WeixinMpException;
	
	/**
	 * 设置备注名
	 * http请求方式: POST（请使用https协议）
		https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=ACCESS_TOKEN
	 * @param openid 用户标识 
	 * @param remark  	新的备注名，长度必须小于30字符 
	 * @param access_token  	调用接口凭证 
	 * @return
	 */
	CommonResp updateRemark(String openid,String remark ,String access_token )throws WeixinMpException;
	
	/**
	 * 
	 * 获取用户基本信息（包括UnionID机制）
	 * http请求方式: GET
		https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN
	 * @param lang  	否 	返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语 
	 * @param openid 是 	普通用户的标识，对当前公众号唯一 
	 * @param access_token 是 	调用接口凭证 
	 * @return
	 * @throws WeixinMpException
	 */
	GetUserInfoResp getWxmpUserInfo(String openid ,String access_token)throws WeixinMpException;
	
	
	/**
	 * 获取用户标签
	 * 一个公众号，最多可以创建100个标签。
	 * http请求方式：POST（请使用https协议）
		https://api.weixin.qq.com/cgi-bin/tags/create?access_token=ACCESS_TOKEN
	 * @param name
	 * @param access_token
	 * @return
	 *   错误码	说明
		-1	系统繁忙
		45157 标签名非法，请注意不能和其他标签重名
		45158	标签名长度超过30个字节
		45056	创建的标签数过多，请注意不能超过100个
	 */
	CreateTagResp createTag(String name,String access_token)throws WeixinMpException;
	
	/**
	 * 获取已创建的标签
	 * @param access_token
	 * @return
	 */
	GetTagsResp getTags(String access_token)throws WeixinMpException;
	
	
	/**
	 * 修改标签
	 * @param id
	 * @param name
	 * @return
	 * 	  错误码	说明
		-1	系统繁忙
		45157	标签名非法，请注意不能和其他标签重名
		45158	标签名长度超过30个字节
		45058	不能修改0/1/2这三个系统默认保留的标签
	 */
	CommonResp updateTag(String id,String name,String access_token)throws WeixinMpException;
	
	
	/**
	 * 删除标签
	 * http请求方式：POST（请使用https协议）
		https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=ACCESS_TOKEN
	 * @param id
	 * @param access_token
	 * @return
	 * 错误码	说明
		-1	系统繁忙
		45058	不能修改0/1/2这三个系统默认保留的标签
		45057       该标签下粉丝数超过10w，不允许直接删除
	 */
	CommonResp delTag(String id,String access_token)throws WeixinMpException;
	
	/**
	 * 跟据标签获得用户
	 * https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=ACCESS_TOKEN
	 * @param tagid
	 * @param next_openid //第一个拉取的OPENID，不填默认从头开始拉取
	 * @param access_token
	 * @return
	 * 错误码	说明
		-1	系统繁忙
		40003	传入非法的openid
		45159	非法的tag_id
	 */
	GetUserByTagidResp getUserByTagid(String tagid,String next_openid,String access_token)throws WeixinMpException;
	
	/**
	 * 批量为用户打标签 
	 * 标签功能目前支持公众号为用户打上最多三个标签。
	 * https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=ACCESS_TOKEN
	 * @param tagid
	 * @param openid_list //粉丝列表
	 * @param access_token
	 * @return
	 * 错误码	说明
		-1	系统繁忙
		40032	每次传入的openid列表个数不能超过50个
		45159	非法的标签
		45059	有粉丝身上的标签数已经超过限制
		40003	传入非法的openid
		49003	传入的openid不属于此AppID
	 */
	CommonResp batchtagging(String tagid,String[]openid_list,String access_token)throws WeixinMpException;
	
	/**
	 * 批量为用户取消标签
	 * https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=ACCESS_TOKEN
	 * @param tagid
	 * @param openid_list
	 * @param access_token
	 * @return
	 * 错误码	说明
		-1	系统繁忙
		40032	每次传入的openid列表个数不能超过50个
		45159	非法的标签
		40003	传入非法的openid
		49003	传入的openid不属于此AppID
	 */
	CommonResp batchuntagging(String tagid,String[]openid_list,String access_token)throws WeixinMpException;
	
	/**
	 * 获取用标签列表
	 * https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token=ACCESS_TOKEN
	 * @param openid
	 * @param access_token
	 * @return
	 * 错误码	说明
		-1	系统繁忙
		40003	传入非法的openid
		49003	传入的openid不属于此AppID
	 */
	GetidlistResp getidlist(String openid ,String access_token)throws WeixinMpException;
	
	/**
	 * 获取用户列表
	 * https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID
	 * @param next_openid 是	第一个拉取的OPENID，不填默认从头开始拉取
	 * @param access_token 是	调用接口凭证
	 * @return
	 * @throws WeixinMpException
	 */
	GetUserListResp getUserList(String next_openid ,String access_token)throws WeixinMpException;
	
	/**
	 * 获取黑名单用户列表
	 * https://api.weixin.qq.com/cgi-bin/tags/members/getblacklist?access_token=ACCESS_TOKEN
	 * @param next_openid //当 begin_openid 为空时，默认从开头拉取。
	 * @param access_token
	 * @return
	 * 返回码	说明
		-1	系统繁忙
		40003	传入非法的openid
		49003	传入的openid不属于此AppID
	 * @throws WeixinMpException
	 */
	GetBlackListResp getBlackList(String next_openid ,String access_token)throws WeixinMpException;
	
	/**
	 * 拉黑用户
	 * https://api.weixin.qq.com/cgi-bin/tags/members/batchblacklist?access_token=ACCESS_TOKEN
	 * @param openid_list 是	需要拉入黑名单的用户的openid，一次拉黑最多允许20个
	 * @param access_token
	 * @return  返回码	说明
		-1	系统繁忙
		40003	传入非法的openid
		49003	传入的openid不属于此AppID
		40032	一次只能拉黑20个用户
	 * @throws WeixinMpException
	 */
	CommonResp batchblacklist(String[]openid_list ,String access_token)throws WeixinMpException;
	
	/**
	 * 取消拉黑用户
	 * https://api.weixin.qq.com/cgi-bin/tags/members/batchunblacklist?access_token=ACCESS_TOKEN
	 * @param openid_list
	 * @param access_token
	 * @return
	 * 返回码	说明
		-1	系统繁忙
		40003	传入非法的openid
		49003	传入的openid不属于此AppID
		40032	一次只能拉黑20个用户
	 * @throws WeixinMpException
	 */
	CommonResp batchunblacklist(String[]openid_list ,String access_token)throws WeixinMpException;
	
	
}




