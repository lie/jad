package com.jad.wx.mp.req;

import java.io.Serializable;
import java.util.List;

public class GetWxmpUserInfoListReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<GetWxmpUserInfoListItemReq>user_list;

	public List<GetWxmpUserInfoListItemReq> getUser_list() {
		return user_list;
	}

	public void setUser_list(List<GetWxmpUserInfoListItemReq> user_list) {
		this.user_list = user_list;
	}
}
