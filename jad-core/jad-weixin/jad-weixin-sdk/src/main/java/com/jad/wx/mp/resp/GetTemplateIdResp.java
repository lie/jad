package com.jad.wx.mp.resp;

/**
 * 获得模板id
 * @author hechuan
 *
 */
public class GetTemplateIdResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String template_id;

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}
	
	

}
