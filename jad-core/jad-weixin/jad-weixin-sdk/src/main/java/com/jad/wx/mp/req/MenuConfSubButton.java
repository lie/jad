package com.jad.wx.mp.req;

import java.io.Serializable;
import java.util.List;

public class MenuConfSubButton implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<MenuConfSubButtonItem>list;

	public List<MenuConfSubButtonItem> getList() {
		return list;
	}

	public void setList(List<MenuConfSubButtonItem> list) {
		this.list = list;
	}
	




}
