package com.jad.wx.mp.resp;

import java.util.List;

/**
 * 获取用标签列表
 * @author hechuan
 *
 */
public class GetidlistResp  extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Integer>tagid_list;

	public List<Integer> getTagid_list() {
		return tagid_list;
	}

	public void setTagid_list(List<Integer> tagid_list) {
		this.tagid_list = tagid_list;
	}
	
}
