package com.jad.wx.mp.resp;

/**
 * 回复视频消息
 * @author hechuan
 *
 */
public class VideoMsgResp  extends MsgReplyResp{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mediaId ;//	是 	通过素材管理接口上传多媒体文件，得到的id
	private String title ;//	否 	视频消息的标题
	private String description ;//	否 	视频消息的描述 
	
	public String getMediaId() {
		return mediaId;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
