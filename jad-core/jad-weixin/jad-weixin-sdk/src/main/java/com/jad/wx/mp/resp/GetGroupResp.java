package com.jad.wx.mp.resp;

import java.util.List;

/**
 * 查询分组
 * @author hechuan
 *
 */
public class GetGroupResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<WxmpGroup> groups;

	public List<WxmpGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<WxmpGroup> groups) {
		this.groups = groups;
	}


}
