package com.jad.wx.mp.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.http.HttpResp;
import com.jad.commons.http.HttpUtil;
import com.jad.commons.http.MyHttpException;
import com.jad.commons.json.JsonMapper;
import com.jad.wx.mp.constant.MpConstant;
import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.req.CreateQrTicketReq;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.CreateQrTicketResp;
import com.jad.wx.mp.resp.GetShortUrlResp;
import com.jad.wx.mp.service.QrCodeService;

public class QrCodeServiceImpl extends AbstractWxServiceImpl implements QrCodeService {
	
	private static Logger logger=LoggerFactory.getLogger(QrCodeServiceImpl.class);
	
//	{"ticket":"gQGD8TwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAySjFXSmRqck1iMlQxMDAwME0wNzQAAgQOmD5YAwQAAAAA",
//	"url":"http://weixin.qq.com/q/02J1WJdjrMb2T10000M074"}
	
	public static void main(String[] args) throws Exception{
		
		CommonResp resp=null;
		QrCodeServiceImpl service=new QrCodeServiceImpl();
		
		String openid="o2zDCt7_YbDAUXBY-fINZ1W4VVkk";
		String access_token=MpConstant.TOKEN;
		
//		CreateQrTicketResp resp=service.createQrTicket(getCreateQrTicketReq(), access_token);
//		System.out.println("收到生成二维码结果,resp:"+JsonMapper.toJsonString(resp));
		
		String ticket="gQGD8TwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAySjFXSmRqck1iMlQxMDAwME0wNzQAAgQOmD5YAwQAAAAA";
//		byte[] showqrcode=service.showqrcode(ticket);
		
//		service.getQrcode(ticket, "F:\\ttt.jpg");
		
		String redirect_uri="http://weixin.bolo28.com/jeesitewx/listvote1.html";
		String long_url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx40659e5cfc5bea25&redirect_uri="+redirect_uri+"&wx_state=0&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";
		resp=service.toShorturl(long_url, access_token);
		
		System.out.println("结果:"+JsonMapper.toJsonString(resp));
		
		
	}
	
	public File getQrcode(String ticket,String filePath)throws WeixinMpException{
		
		String url="https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket="+ticket;
		
		try {
			
			File file=HttpUtil.getToFile(url, filePath);
			
			return file;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
	}
	
	public static CreateQrTicketReq getCreateQrTicketReq(){
		CreateQrTicketReq req=new CreateQrTicketReq();
		
		req.setAction_name(MpConstant.QR_LIMIT_STR_SCENE);;
		req.setScene_str("tttdddddddddfff");
		
		return req;
	}
	
	@Override
	public CreateQrTicketResp createQrTicket(CreateQrTicketReq req, String access_token) throws WeixinMpException {
		
		Map<String,Object>params=new HashMap<String,Object>();
		Map<String,Object>actionInfoMap=new HashMap<String,Object>();
		
		params.put("action_info", actionInfoMap);
		
		Map<String,String>sceneMap=new HashMap<String,String>();
		
		actionInfoMap.put("scene", sceneMap);
		
		params.put("action_name", req.getAction_name());
		
		if(MpConstant.QR_SCENE.equals(req.getAction_name())){//临时二维码
			
			
			if(StringUtils.isNotBlank(req.getExpire_seconds())){
				params.put("expire_seconds", req.getExpire_seconds());
				
			}
			
		}else if(MpConstant.QR_LIMIT_SCENE.equals(req.getAction_name())){//
			sceneMap.put("scene_id", req.getScene_id());
		}else{
			sceneMap.put("scene_str", req.getScene_str());
		}
		
		
		String url=MpConstant.API_URL+"/qrcode/create?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			CreateQrTicketResp resp=(CreateQrTicketResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CreateQrTicketResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}
	
	public GetShortUrlResp toShorturl(String long_url,String access_token) throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/shorturl?access_token="+access_token;
		
		try {
			Map<String,String>params=new HashMap<String,String>();
			params.put("action", "long2short");
			params.put("long_url", long_url);
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			GetShortUrlResp resp=(GetShortUrlResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetShortUrlResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
	}

	@Override
	public byte[] showqrcode(String ticket) throws WeixinMpException {
		
		String url="https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket="+ticket;
		
		try {
			
			byte[]qrData=HttpUtil.getByte(url);
			
			return qrData;
			
		} catch (MyHttpException e) {
			
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		
	}

}
