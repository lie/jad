/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.oa.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.commons.util.QoUtil;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.core.oa.dao.NotifyDao;
import com.jad.core.oa.dao.NotifyRecordDao;
import com.jad.core.oa.entity.NotifyEo;
import com.jad.core.oa.entity.NotifyRecordEo;
import com.jad.core.oa.qo.NotifyQo;
import com.jad.core.oa.qo.NotifyRecordQo;
import com.jad.core.oa.service.NotifyService;
import com.jad.core.oa.vo.NotifyRecordVo;
import com.jad.core.oa.vo.NotifyVo;
import com.jad.dao.utils.EntityUtils;

/**
 * 通知通告Service
 */
@Service("notifyService")
@Transactional(readOnly = true)
public class NotifyServiceImpl extends AbstractServiceImpl<NotifyEo, String,NotifyVo> implements NotifyService{
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private NotifyDao notifyDao;
	
	@Autowired
	private NotifyRecordDao notifyRecordDao;

	/**
	 * 获取通知发送记录
	 * @param oaNotify
	 * @return
	 */
	public List<NotifyRecordVo> getRecordList(String notifyId) {
		
		Assert.isTrue(StringUtils.isNotBlank(notifyId));
		
		NotifyRecordQo rqo = QoUtil.newNormalDataQo(NotifyRecordQo.class);
		
		rqo.setNotifyId(notifyId);
		
		return EntityUtils.copyToVoList(notifyRecordDao.findByQo(rqo), NotifyRecordVo.class);
	}
	
	
	
	public Page<NotifyVo> findUserNodify(PageQo page, NotifyQo notifyQo,String userId) {
		if(notifyQo==null){
			notifyQo = QoUtil.newNormalDataQo(NotifyQo.class);
		}
		return EntityUtils.copyToVoPage(notifyDao.findUserNodify(page, QoUtil.wrapperNormalDataQo(notifyQo), userId), NotifyVo.class);
	}
	
	/**
	 * 获取用户通知数目
	 * @param oaNotify
	 * @return
	 */
	public Long findUserNodifyCount(NotifyQo notifyQo ,String userId) {
		Assert.isTrue(StringUtils.isNotBlank(userId));
		if(notifyQo==null){
			notifyQo = QoUtil.newNormalDataQo(NotifyQo.class);
		}
		return notifyDao.findUserNodifyCount(QoUtil.wrapperNormalDataQo(notifyQo),userId);
	}
	
	
	/**
	 * 更新阅读状态
	 */
	@Transactional(readOnly = false)
	public void updateReadFlag(String notifyId) {
		
		Assert.isTrue(StringUtils.isNotBlank(notifyId));
		
		NotifyRecordEo eo = new NotifyRecordEo();
		eo.setReadFlag("1");
		eo.setReadDate(new Date());
		eo.setUpdateDate(new Date());
		
		NotifyRecordQo qo = new NotifyRecordQo();
		qo.setNotifyId(notifyId);
		
		notifyRecordDao.updateByQo(eo, qo);
		
//		String sql="update oa_notify_record set read_flag=?,read_date= str_to_date(?,'%Y-%m-%d %H:%i:%s')"
//				+ ",  update_date = str_to_date(?,'%Y-%m-%d %H:%i:%s') where notify_id=?";
//		String dateStr=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
//		List params=new ArrayList();
//		params.add("1");
//		params.add(dateStr);
//		params.add(dateStr);
//		params.add(notifyId);
//		notifyRecordDao.executeSql(sql, params);
		
	}

}

