package com.jad.commons.encrypt;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import com.jad.commons.utils.ByteUtils;
import com.jad.commons.utils.Exceptions;

/**
 * des工具，这个方法有bug，需要再测试
 * @author Administrator
 *
 */
public class DesUtil {
	
	private static final String DES = "DES";
	
	private static final String DEFAULT_URL_ENCODING = "UTF-8";
	
	
//	PKCS7Padding跟PKCS5Padding的区别就在于数据填充方式，PKCS7Padding是缺几个字节就补几个字节的0，而PKCS5Padding是缺几个字节就补充几个字节的几，好比缺6个字节，就补充6个字节的6 
	
	public static final String DEFAULT_PADDING_MODEL = "DES/CBC/PKCS5Padding";
	
	public static final String NO_PADDING_MODEL = "DES/CBC/NoPadding";//不填充  //"DES/ECB/NoPadding"
	
	public static void main(String[] args) throws Exception{
		String src="12345678";
		String key="1234567812345678";
		
		String enStr=desEncrypt(src,key);
		System.out.println("加密后1:"+enStr);
		
		byte[]keyByte=ByteUtils.hexString2Bytes("74D0E04F1C335AEF");
		
		enStr=Encodes.encodeHex(desEncrypt(src.getBytes(DEFAULT_URL_ENCODING), keyByte,NO_PADDING_MODEL));
		System.out.println("加密后2:"+enStr);
		
		enStr=desEncrypt(src,key,NO_PADDING_MODEL);
		System.out.println("加密后3:"+enStr);
		
//		String srcStr=desDecrypt(enStr,key);
//		System.out.println("解密后:"+srcStr);
		
	}
	/**
	 * 使用DES加密原始字符串.
	 * 
	 * @param input 
	 * @param key 
	 */
	public static String desEncrypt(String input, String key,String paddingModel) {
		try {
			return Encodes.encodeHex(desEncrypt(input.getBytes(DEFAULT_URL_ENCODING), Encodes.decodeHex(key),paddingModel));
		} catch (UnsupportedEncodingException e) {
			throw Exceptions.unchecked(e);
		}
	}
	
	/**
	 * 使用AES解密字符串, 返回原始字符串.
	 * 
	 * @param input 
	 * @param key 
	 */
	public static String desDecrypt(String input, String key,String paddingModel) {
		try {
			return new String(desDecrypt(Encodes.decodeHex(input), Encodes.decodeHex(key),paddingModel), DEFAULT_URL_ENCODING);
		} catch (UnsupportedEncodingException e) {
			throw Exceptions.unchecked(e);
		}
	}
	
	
	/**
	 * 使用DES加密原始字符串.
	 * 
	 * @param input 
	 * @param key 
	 */
	public static String desEncrypt(String input, String key) {
		try {
			return Encodes.encodeHex(desEncrypt(input.getBytes(DEFAULT_URL_ENCODING), Encodes.decodeHex(key),DEFAULT_PADDING_MODEL));
		} catch (UnsupportedEncodingException e) {
			throw Exceptions.unchecked(e);
		}
	}
	
	/**
	 * 使用AES解密字符串, 返回原始字符串.
	 * 
	 * @param input 
	 * @param key 
	 */
	public static String desDecrypt(String input, String key) {
		try {
			return new String(desDecrypt(Encodes.decodeHex(input), Encodes.decodeHex(key),DEFAULT_PADDING_MODEL), DEFAULT_URL_ENCODING);
		} catch (UnsupportedEncodingException e) {
			throw Exceptions.unchecked(e);
		}
	}
	
	
	/**
	 * 使用des加密原始字符串.
	 * 
	 * @param input 原始输入数组
	 * @param key 符合AES要求的密钥
	 */
	public static byte[] desEncrypt(byte[] input, byte[] key,String paddingModel) {
		return des(input, key, Cipher.ENCRYPT_MODE,paddingModel);
	}

	
	/**
	 * 使用DES解密字符串, 返回原始字符串.
	 * 
	 * @param input 原始输入数组
	 * @param key 符合DES要求的密钥
	 */
	public static byte[] desDecrypt(byte[] input, byte[] key,String paddingModel) {
		return des(input, key, Cipher.DECRYPT_MODE,paddingModel);
	}
	
	
	
	/**
	 * 使用DES加密或解密无编码的原始字节数组, 返回无编码的字节数组结果.
	 * 
	 * @param input 原始字节数组
	 * @param key 符合DES要求的密钥
	 * @param mode Cipher.ENCRYPT_MODE 或 Cipher.DECRYPT_MODE
	 */
	private static byte[] des(byte[] input, byte[] key, int mode,String paddingModel) {
		try {
			// DES算法要求有一个可信任的随机数源
			SecureRandom random = new SecureRandom();
			// 创建一个DESKeySpec对象
			DESKeySpec desKey = new DESKeySpec(key);
			// 创建一个密匙工厂
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
			// 将DESKeySpec对象转换成SecretKey对象
			SecretKey securekey = keyFactory.generateSecret(desKey);
			// Cipher对象实际完成解密操作
			Cipher cipher=null;
			if(Cipher.ENCRYPT_MODE==mode){
//				cipher=Cipher.getInstance(paddingModel);  
				cipher=Cipher.getInstance(DEFAULT_PADDING_MODEL);  
			}else{
				cipher=Cipher.getInstance("DES");  
//				DEFAULT_PADDING_MODEL
//				cipher=Cipher.getInstance(DEFAULT_PADDING_MODEL);
			}
			// 用密匙初始化Cipher对象
			cipher.init(mode, securekey, random);
			
//			byte[] iv = new sun.misc.BASE64Decoder().decodeBuffer("t4JPbY+rXgk=");
//			javax.crypto.spec.IvParameterSpec ips = new javax.crypto.spec.IvParameterSpec(iv);
//			cipher.init(Cipher.ENCRYPT_MODE, key,ips);
			
			// 真正开始解密操作
			return cipher.doFinal(input);
		} catch (GeneralSecurityException e) {
			throw Exceptions.unchecked(e);
		}
	}

	
	

	
	

}
