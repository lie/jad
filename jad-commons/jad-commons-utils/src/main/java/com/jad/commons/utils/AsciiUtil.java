package com.jad.commons.utils;

public class AsciiUtil {
	
	public static void main(String[] args) {

		byte[]bytes=new byte[]{48,49,50,51,52,53};
		
		System.out.println(byteToAscii(bytes));
	}
	
	public static String byteToAscii(byte[]bytes){
		StringBuffer sb=new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			char c=(char)bytes[i];
			sb.append(c);
		}
		return sb.toString();
	}
	
}
