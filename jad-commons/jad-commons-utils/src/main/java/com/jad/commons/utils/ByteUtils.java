package com.jad.commons.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 字节工具类
 * 
 * @author Administrator
 *
 */
public class ByteUtils {

	private static Logger logger = LoggerFactory.getLogger(ByteUtils.class);

	private static String hexStr = "0123456789ABCDEF";

	private static String[] binaryArray = { "0000", "0001", "0010", "0011",
			"0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011",
			"1100", "1101", "1110", "1111" };

	public static void main(String[] args) throws Exception {
		byte[] bs = hexString2Bytes("0020000000C00016");
		// byte[]bs="0020000000C00016".getBytes("UTF-8");
		System.out.println(bytes2BinaryStr(bs));
		System.out.println(bytes2HexString(bs));
		// System.out.println(bytes2String(bs,0,bs.length,"UTF-8"));
	}

	/**
	 * 
	 * @param str
	 * @return 转换为二进制字符串
	 */
	public static String bytes2BinaryStr(byte[] bArray) {

		String outStr = "";
		int pos = 0;
		for (byte b : bArray) {
			// 高四位
			pos = (b & 0xF0) >> 4;
			outStr += binaryArray[pos];
			// 低四位
			pos = b & 0x0F;
			outStr += binaryArray[pos];
		}
		return outStr;

	}

	/**
	 *
	 * @param bytes
	 * @return 将二进制转换为十六进制字符输出
	 */
	public static String bytes2HexString(byte[] bytes) {

		String result = "";
		String hex = "";
		for (int i = 0; i < bytes.length; i++) {
			// 字节高4位
			hex = String.valueOf(hexStr.charAt((bytes[i] & 0xF0) >> 4));
			// 字节低4位
			hex += String.valueOf(hexStr.charAt(bytes[i] & 0x0F));
			result += hex + "";
		}
		return result;
	}

	// public static String bytes2HexString2(byte[] src) {
	//
	// StringBuilder stringBuilder = new StringBuilder("");
	// if (src == null || src.length <= 0) {
	// return null;
	// }
	// for (int i = 0; i < src.length; i++) {
	// int v = src[i] & 0xFF;
	// String hv = Integer.toHexString(v);
	// if (hv.length() < 2) {
	// stringBuilder.append(0);
	// }
	// stringBuilder.append(hv);
	// }
	// return stringBuilder.toString();
	//
	// }

	public static String bytes2String(byte[] src, int begin, int length,
			String character) {
		return bytes2String(src, begin, length, character, false);
	}

	public static String bytes2String(byte[] src, int begin, int length,
			String character, boolean limit) {

		byte[] res = getByteArray(src, begin, length);

		String resStr = null;
		try {
			resStr = new String(res, character);
		} catch (UnsupportedEncodingException e) {
			logger.error("字符串转换失败," + e.getMessage(), e);
			resStr = new String(res);
		}
		if (resStr != null && limit) {
			resStr = resStr.trim();
		}
		return resStr;
	}

	public static byte[] getByteArray(byte[] src, int begin, int length) {
		byte[] res = new byte[length];

		for (int i = begin; i < length + begin; i++) {
			res[i - begin] = src[i];
		}

		return res;
	}

	public static byte[] input2ByteArray(InputStream input, int lenByteSize)
			throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		byte[] lenBuf = new byte[lenByteSize];
		int totalLen = 0;
		int n = 0;
		int c = 0;
		while (-1 != (n = input.read())) {
			output.write((byte)n);
			c++;
			if (c <= lenByteSize) {
				lenBuf[c - 1] = (byte) n;
				if (c == lenByteSize) {
					// String len16 = ByteUtil.bytesToHexString(lenBuf);
					String len16 = bytes2HexString(lenBuf);
					totalLen = Integer.parseInt(len16, 16);
				}
			}
			if (totalLen > 0 && c >= totalLen + lenByteSize) {
				break;
			}
		}
		return output.toByteArray();
	}

	/**
	 *
	 * @param iSource
	 * @param iArrayLen
	 * @return
	 */
	public static byte[] int2ByteArray(int iSource, int iArrayLen) {
		byte[] bLocalArr = new byte[iArrayLen];
		for (int i = iArrayLen; (i < 4) && (i > 0); i--) {
			bLocalArr[i - 1] = (byte) (iSource >> 8 * (iArrayLen - i) & 0xFF);
		}
		return bLocalArr;
	}

	/**
	 * 
	 * @param hexString
	 * @return 将十六进制转换为字节数组
	 */
	public static byte[] hexString2Bytes(String hexString) {
		// hexString的长度对2取整，作为bytes的长度
		int len = hexString.length() / 2;
		byte[] bytes = new byte[len];
		byte high = 0;// 字节高四位
		byte low = 0;// 字节低四位

		for (int i = 0; i < len; i++) {
			// 右移四位得到高位
			high = (byte) ((hexStr.indexOf(hexString.charAt(2 * i))) << 4);
			low = (byte) hexStr.indexOf(hexString.charAt(2 * i + 1));
			bytes[i] = (byte) (high | low);// 高地位做或运算
		}
		return bytes;
	}

	public static byte[] byteAndByte(byte[] begin, byte[] second) {

		if (begin == null || begin.length == 0) {

			if (second != null && second.length != 0) {

				return second;

			} else {

				return null;
			}

		} else if (second == null || second.length == 0) {

			return begin;
		}

		byte[] newTotal = new byte[begin.length + second.length];

		for (int i = 0; i < begin.length; i++) {

			newTotal[i] = begin[i];
		}

		for (int i = begin.length; i < second.length + begin.length; i++) {

			newTotal[i] = second[i - begin.length];
		}

		return newTotal;
	}

}
