package com.jad.commons.utils;

public enum StringEncoding {

	GB2312("GB2312", "GB2312"),
	UTF8("UTF-8", "UTF-8"),
	GBK("GBK", "GBK"),
	ISO_8859_1("ISO-8859-1", "ISO-8859-1")
	;
	
	private final String name;

	private final String desc;

	StringEncoding(final String name, final String desc) {
		this.name = name;
		this.desc = desc;
	}
	
	
}
