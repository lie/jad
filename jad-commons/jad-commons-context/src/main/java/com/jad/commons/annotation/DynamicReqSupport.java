package com.jad.commons.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jad.commons.vo.DynamicPojo;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DynamicReqSupport {
	
	Class<?> eoClass() default DynamicPojo.class;
	
	Class<?> voClass() default DynamicPojo.class;
	
	Class<?> qoClass() default DynamicPojo.class;
	
	String serviceBeanName()default "";
	
	String daoBeanName()default "";
	
	String listUrl() default "modules/dynamic/dynamicList";
	
	String formUrl() default "modules/dynamic/dynamicForm";
}




