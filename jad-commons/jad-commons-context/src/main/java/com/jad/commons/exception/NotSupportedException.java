package com.jad.commons.exception;

public class NotSupportedException extends RuntimeException {

	public NotSupportedException() {
		// TODO Auto-generated constructor stub
	}

	public NotSupportedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotSupportedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NotSupportedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotSupportedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
