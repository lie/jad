package com.jad.rpc.provider.cms;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.jad.rpc.service.RpcMain;



/**
 * cms模块启动入口
 * @author Administrator
 *
 */
public class RpcMainCms extends RpcMain{
	
	public static void main(String[] args) {
		start(args);

	}

}
