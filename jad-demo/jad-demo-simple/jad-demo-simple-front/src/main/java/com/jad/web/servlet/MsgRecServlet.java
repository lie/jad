package com.jad.web.servlet;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;


//http://67.229.165.10/testWeixin/MsgRecServlet
public class MsgRecServlet extends HttpServlet {

	private static final Logger logger = Logger.getLogger(MsgRecServlet.class);

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		System.out.println("收到get请求");
		doPost(request, response);
	}

	/*
	 * 回复 <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
	 * <FromUserName><![CDATA[fromUser]]></FromUserName>
	 * <CreateTime>12345678</CreateTime> <MsgType><![CDATA[text]]></MsgType>
	 * <Content><![CDATA[你好]]></Content> </xml>
	 */

	/*
	 * 收到 <xml><ToUserName><![CDATA[gh_e3a4c365efec]]></ToUserName>
	 * <FromUserName><![CDATA[oSZ7FjhWM7MNao4nJIV-I5hcWF8U]]></FromUserName>
	 * <CreateTime>1460137452</CreateTime> <MsgType><![CDATA[text]]></MsgType>
	 * <Content><![CDATA[把]]></Content> <MsgId>6271242604408017104</MsgId>
	 * </xml>
	 */

	public static void main(String[] args) throws JDOMException, IOException {
		String recMsg = "<xml><ToUserName><![CDATA[gh_e3a4c365efec]]></ToUserName>"
				+ "<FromUserName><![CDATA[oSZ7FjhWM7MNao4nJIV-I5hcWF8U]]></FromUserName>"
				+ "<CreateTime>1460137452</CreateTime>"
				+ "<MsgType><![CDATA[text]]></MsgType>"
				+ "<Content><![CDATA[把]]></Content>"
				+ "<MsgId>6271242604408017104</MsgId>" + "</xml>";

		InputStream input = new ByteArrayInputStream(recMsg.getBytes());
		SAXBuilder builder = new SAXBuilder();
		Document document = builder.build(input);
		Element root = document.getRootElement();// 获得根节点
		
		System.out.println(root.getChildText("MsgType"));
		System.out.println(root.toString());

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		String msg = "";
		if ("text/xml".equals(request.getContentType())) {
			
			try {
				
				StringWriter sw = new StringWriter();
				IOUtils.copy(request.getInputStream(), sw, "utf-8");
				String xml = sw.toString();
				System.out.println("收到来自公众号的xml消息,xml:" + xml);
				
				InputStream input = new ByteArrayInputStream(xml.getBytes());
				SAXBuilder builder = new SAXBuilder();
				Document document = builder.build(input);
				Element root = document.getRootElement();// 获得根节点
				
				String msgType=root.getChildText("MsgType");
				
//				RecTextMsg recMsg=RecTextMsg.fromXml(root);
//				if("text".equals(msgType)){//收到文本消息
//					msg="正在处理文本消息:"+recMsg.getContent();
//				}else{
//					msg="目前只支持文本消息";
//				}
//				SendTextMsg sendMsg=new SendTextMsg();
//				sendMsg.setToUserName(recMsg.getFromUserName());
//				sendMsg.setFromUserName(recMsg.getToUserName());
//				sendMsg.setCreateTime(recMsg.getCreateTime());
//				sendMsg.setMsgType("text");
//				sendMsg.setContent(msg);
//				msg=sendMsg.toXml();
				
				System.out.println("准备回复:"+msg);
				
			} catch (Exception e) {
				System.out.println("exception,"+e.getMessage());
				logger.error("微信请求失败,"+e.getMessage(),e);
				e.printStackTrace();
				msg="微信请求失败";
			}catch(Throwable e){
				System.out.println("Throwable,"+e.getMessage());
				e.printStackTrace();
			}
		} else {
			// 测试验证
			String signature = request.getParameter("signature");// 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
			String timestamp = request.getParameter("timestamp");// 时间戳
			String nonce = request.getParameter("nonce");// 随机数
			String echostr = request.getParameter("echostr");// 随机字符串
			msg = echostr==null?"随机数":echostr;
			boolean check = validate(signature, timestamp, nonce);
			if (!check) {
				logger.warn("非法请求,signature:" + signature + ",timestamp:"
						+ timestamp + ",nonce:" + nonce + ",echostr:" + echostr);
				msg="非法请求,signature:" + signature + ",timestamp:"
						+ timestamp + ",nonce:" + nonce + ",echostr:" + echostr;
			}
		}

		try {
			response.getWriter().write(msg);
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

//	protected void doPost_bak_ok(HttpServletRequest request, HttpServletResponse response) {
//		String msg = "";
//		if ("text/xml".equals(request.getContentType())) {
//			StringWriter sw = new StringWriter();
//			try {
//				MsgIoUtil.copy(request.getInputStream(), sw, "utf-8");
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			String xml = sw.toString();
//			System.out.println("收到来自公众号的xml消息,xml:" + xml);
//
//			String toUserName = xml.substring(
//					xml.indexOf("<ToUserName><![CDATA[")
//							+ "<ToUserName><![CDATA[".length(),
//					xml.indexOf("]]></ToUserName>"));
//			String fromUserName = xml.substring(
//					xml.indexOf("<FromUserName><![CDATA[")
//							+ "<FromUserName><![CDATA[".length(),
//					xml.indexOf("]]></FromUserName>"));
//			String content = xml.substring(xml.indexOf("<Content><![CDATA[")
//					+ "<Content><![CDATA[".length(),
//					xml.indexOf("]]></Content>"));
//
//			System.out.println("toUserName:" + toUserName + "\nfromUserName:"
//					+ fromUserName + "\ncontent:" + content);
//
//			String reXml = "<xml>"
//					+ "<ToUserName><![CDATA[{toUser}]]></ToUserName>"
//					+ "<FromUserName><![CDATA[{fromUser}]]></FromUserName>"
//					+ "<CreateTime>12345678</CreateTime>"
//					+ "<MsgType><![CDATA[text]]></MsgType>"
//					+ "<Content><![CDATA[{rescontent}]]></Content>" + "</xml>";
//			reXml = reXml.replace("{toUser}", fromUserName);
//			reXml = reXml.replace("{fromUser}", toUserName);
//			reXml = reXml.replace("{rescontent}", "hello");
//
//			msg = reXml;
//
//			System.out.println(reXml);
//
//		} else {
//
//			String signature = request.getParameter("signature");// 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
//			String timestamp = request.getParameter("timestamp");// 时间戳
//			String nonce = request.getParameter("nonce");// 随机数
//			String echostr = request.getParameter("echostr");// 随机字符串
//			msg = echostr;
//			// String signature, String timestamp, String nonce
//			if (validate(signature, timestamp, nonce)) {
//				if (echostr == null || "".equals(echostr.trim())) {
//					msg = "<xml>"
//							+ "<ToUserName><![CDATA[toUser]]></ToUserName>"
//							+ "<FromUserName><![CDATA[fromUser]]></FromUserName>"
//							+ "<CreateTime>12345678</CreateTime>"
//							+ "<MsgType><![CDATA[text]]></MsgType>"
//							+ "<Content><![CDATA[hello]]></Content>" + "</xml>";
//				} else {
//					msg = echostr;
//				}
//
//			} else {
//				msg = "<xml>" + "<ToUserName><![CDATA[toUser]]></ToUserName>"
//						+ "<FromUserName><![CDATA[fromUser]]></FromUserName>"
//						+ "<CreateTime>12345678</CreateTime>"
//						+ "<MsgType><![CDATA[text]]></MsgType>"
//						+ "<Content><![CDATA[hello]]></Content>" + "</xml>";
//			}
//
//			System.out.println("收到来自公众号的消息,signature:" + signature
//					+ ",timestamp:" + timestamp + ",nonce:" + nonce
//					+ ",echostr:" + echostr);
//		}
//
//		try {
//			response.getWriter().write(msg);
//			response.getWriter().flush();
//			response.getWriter().close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//	}

	// protected void doPost_bak(HttpServletRequest request, HttpServletResponse
	// response) {
	//
	// // String username=request.getParameter("token");
	// // Map map=request.getParameterMap();
	// // Object obj=request.getAttributeNames();
	//
	//
	// StringWriter sw = new StringWriter();
	// try {
	// MyIoUtil.copy(request.getInputStream(), sw, "utf-8");
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// String xml=sw.toString();
	//
	// // System.out.println("收到post请求,username:"+username);
	// System.out.println("请求内容:"+xml);
	//
	// String msg="{\"success\":\"0000\"}";
	// try {
	// response.getWriter().write(msg);
	// response.getWriter().flush();
	// response.getWriter().close();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	//
	// }

	// 收到来自公众号的消息,signature:1cd986748fa5eae3d07a3f1379c24abefdf95484,timestamp:1460134693,nonce:1209114982,echostr:2047792132774689575
	// 收到来自公众号的消息,signature:9e60028de38b7a9497f301e8fe2382a8a79ab7a2,timestamp:1460134705,nonce:2101829229,echostr:989048356180321271

	// public static void main(String[] args) {
	// String signature="1cd986748fa5eae3d07a3f1379c24abefdf95484";
	// String timestamp="1460134693";
	// String nonce="1209114982";
	// String echostr="2047792132774689575";
	//
	// String xml="<xml><ToUserName><![CDATA[gh_e3a4c365efec]]></ToUserName>"
	// +"<FromUserName><![CDATA[oSZ7FjhWM7MNao4nJIV-I5hcWF8U]]></FromUserName>"
	// +"<CreateTime>1460137452</CreateTime>"
	// +"<MsgType><![CDATA[text]]></MsgType>"
	// +"<Content><![CDATA[把]]></Content>"
	// +"<MsgId>6271242604408017104</MsgId>"
	// +"</xml>";
	// String
	// toUserName=xml.substring(xml.indexOf("<ToUserName><![CDATA[")+"<ToUserName><![CDATA[".length(),
	// xml.indexOf("]]></ToUserName>"));
	// String
	// fromUserName=xml.substring(xml.indexOf("<FromUserName><![CDATA[")+"<FromUserName><![CDATA[".length(),
	// xml.indexOf("]]></FromUserName>"));
	// String
	// content=xml.substring(xml.indexOf("<Content><![CDATA[")+"<Content><![CDATA[".length(),
	// xml.indexOf("]]></Content>"));
	// System.out.println("toUserName:"+toUserName+"\nfromUserName:"+fromUserName+"\ncontent:"+content);
	//
	// String reXml="<xml>"
	// +"<ToUserName><![CDATA[{toUser}]]></ToUserName>"
	// +"<FromUserName><![CDATA[{fromUser}]]></FromUserName>"
	// +"<CreateTime>12345678</CreateTime>"
	// +"<MsgType><![CDATA[text]]></MsgType>"
	// +"<Content><![CDATA[{rescontent}]]></Content>"
	// +"</xml>";
	// reXml=reXml.replace("{toUser}", fromUserName);
	// reXml=reXml.replace("{fromUser}", toUserName);
	// reXml=reXml.replace("{rescontent}", "hello");
	//
	// System.out.println(reXml);
	//
	//
	// }

	/**
	 * 将字节数组转换成16进制字符串
	 * 
	 * @param b
	 * @return
	 */
	private static String byte2hex(byte[] b) {
		StringBuilder sbDes = new StringBuilder();
		String tmp = null;
		for (int i = 0; i < b.length; i++) {
			tmp = (Integer.toHexString(b[i] & 0xFF));
			if (tmp.length() == 1) {
				sbDes.append("0");
			}
			sbDes.append(tmp);
		}
		return sbDes.toString();
	}

	private static String encrypt(String strSrc) {

		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-1");

			String strDes = null;
			byte[] bt = strSrc.getBytes();
			digest.update(bt);
			strDes = byte2hex(digest.digest());
			return strDes;

		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}
		return null;

	}

	/**
	 * 校验请求的签名是否合法
	 * 
	 * 加密/校验流程： 1. 将token、timestamp、nonce三个参数进行字典序排序 2. 将三个参数字符串拼接成一个字符串进行sha1加密
	 * 3. 开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
	 * 
	 * @param signature
	 * @param timestamp
	 * @param nonce
	 * @return
	 */
	public static boolean validate(String signature, String timestamp,
			String nonce) {
		if (signature == null || timestamp == null || nonce == null) {
			return false;
		}
		// 1. 将token、timestamp、nonce三个参数进行字典序排序
		String token = getToken();
		String[] arrTmp = { token, timestamp, nonce };
		Arrays.sort(arrTmp);
		StringBuffer sb = new StringBuffer();
		// 2.将三个参数字符串拼接成一个字符串进行sha1加密
		for (int i = 0; i < arrTmp.length; i++) {
			sb.append(arrTmp[i]);
		}
		String expectedSignature = encrypt(sb.toString());
		// 3. 开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
		if (expectedSignature.equals(signature)) {
			return true;
		}
		return false;
	}

	private static String getToken() {
		return "idreamtree";
	}

}
