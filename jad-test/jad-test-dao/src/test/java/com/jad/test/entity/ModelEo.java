package com.jad.test.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.jad.commons.vo.EntityObject;


@Entity
@Table(name = "test_model")
public class ModelEo implements EntityObject{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id",updatable=false)
	private String id;
	
	@Column(name="varchar_type")
	private String varcharType;
	
	@OrderBy("int_type asc")
	@Column(name="int_type")
	private Integer intType;
	
	@Column(name="double_type")
	private Double doubleType;
	
	@Column(name="bolb_type")
	private Byte[] bolbType;
	
	@Column(name="text_type")
	private String textType;
	
	@Column(name="boolean_type")
	private Boolean booleanType;
	
	@Column(name="create_by")
	private String createBy;
	
	@Column(name="create_date",updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	@Column(name="update_by")
	private String updateBy;
	
	@Column(name="update_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="del_flag")
	private String delFlag;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVarcharType() {
		return varcharType;
	}

	public void setVarcharType(String varcharType) {
		this.varcharType = varcharType;
	}

	public Integer getIntType() {
		return intType;
	}

	public void setIntType(Integer intType) {
		this.intType = intType;
	}

	public Double getDoubleType() {
		return doubleType;
	}

	public void setDoubleType(Double doubleType) {
		this.doubleType = doubleType;
	}

	public Byte[] getBolbType() {
		return bolbType;
	}

	public void setBolbType(Byte[] bolbType) {
		this.bolbType = bolbType;
	}

	public String getTextType() {
		return textType;
	}

	public void setTextType(String textType) {
		this.textType = textType;
	}

	public Boolean getBooleanType() {
		return booleanType;
	}

	public void setBooleanType(Boolean booleanType) {
		this.booleanType = booleanType;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	
	
	
	
}
